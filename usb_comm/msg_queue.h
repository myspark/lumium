/**
 * @file	: msg_queue.h
 * @date	: 2013-11-13
 * @author	: Pratik Rathod
 * @brief	: Structure,Macro & function declarations for fifo message queue 
 *            operations.
 **/

#ifndef H_MSG_QUEUE
#define H_MSG_QUEUE

//! maximum data length to be stored as message in queue.
#define MAX_DATA		1024

//! fifo queue data structure.
typedef struct queue {
	struct queue *next; //! pointer to next queue structure
	int data_size; //! no of bytes actually stored in data buffer.
	char data[MAX_DATA]; //! Buffer for data to be stored.
} queue_data ;

//! fifo queue information structure.
typedef struct list {
	queue_data *head; //! pointer to head of the queue data structure
	queue_data *tail; //! pointer to tail of the queue data structure
} queue_info ;

/**
 * @brief : Initializes a fifo message queue. It will use malloc, so please free
 *           the returned pointer.
 * @returns : On successful operation it will return the pointer to fifo queue info
 *            structure. On error NULL. 
 */
queue_info *msg_queue_create();

/**
 * @brief : Adds a new entry at the tail of fifo queue.
 * @param	: q [in] pointer to fifo queue info structure
 * @param	: data [in] buffer containing data to be added
 * @param	: size [in] size of buffer 
 * @returns : On successful operation returns EXIT_SUCCESS else EXIT_FAILURE
 */
int msg_queue_add_data(queue_info *q, char *data, int size);

/**
 * @brief : Reads the data from head of the fifo queue and stores it to data buffer
 *          pointer.
 * @param	: q [in] pointer to fifo queue info structure
 * @param	: data [out] data buffer to load the read data from head
 * @param	: size [in] size of data buffer 
 * @returns : On successful operation returns EXIT_SUCCESS else EXIT_FAILURE
 */
int msg_queue_read_head(queue_info *q, char *data, int size);

/**
 * @brief : Frees the head entry from fifo queue and updates the head pointer.
 * @param	: q [in] pointer to fifo queue info structure
 * @returns : On successful operation returns EXIT_SUCCESS else EXIT_FAILURE.
 */
int msg_queue_del_head(queue_info *q);

/**
 * @brief : Flushes whole fifo queue from memory
 * @param	: q [in] pointer to fifo queue info structure
 */
void msg_queue_flush(queue_info *q);

/**
 * @brief : Prints whole fifo queue data.
 * @param	: q [in] pointer to fifo queue structure
 */
void msg_queue_print(queue_info *q);

#endif
