/**
 * @file	: main.c
 * @date	: 2013-11-13
 * @author	: Pratik Rathod
 * @brief	: Main application functions.
 **/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include "common.h"
#include "tcp_comm.h"
#include "msg_queue.h"
#include "loops.h"
#include "config.h"

//! message queue head/tail info pointer
queue_info *g_qinfo = NULL;

//! external receive loop control flag
extern int g_recv_loop_control;
//! external respond loop control flag
extern int g_res_loop_control;
//! external uart loop control flag
extern int g_uart_loop_control;

/**
 * @brief : main entry point function of application
 * @param	: argc [in] no of arguments
 * @param	: argv [in] command line arguments
 * @returns : On successful operation returns EXIT_SUCCESS else EXIT_FAILURE
 */
int main(int argc, char *argv[])
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int sockfd;
//	char ip[16];
//	int port = PORT;
	pthread_t recv_tid;//res_tid;
#if 0
/*****************************************************************************
 *	Application usage (command line args description)
 *	./usb_tcp <IP_ADD> <PORT>
 * If not supplied, it will use default.
 *****************************************************************************/	
	switch(argc)
	{
		case 2 : 
			strncpy(ip,argv[1],16);
			break;
			
		case 3 :
			strncpy(ip,argv[1],16);
			port = atoi(argv[2]);
			if(port==0)
			{
				LOGE("[%s:%d:%s] Invalid port\n",__FILE__,__LINE__,__func__);
				return EXIT_FAILURE;
			}
			break;
			
		default:
			strncpy(ip,IP,16);
			port = PORT ;
			break;
	}
	LOGD("[%s:%d:%s] Using %s:%d for connection...\n",__FILE__,__LINE__,__func__,ip,port);
#endif
	config_init();
retry_connect:	
	//open and connect tcp client socket
	sockfd = tcp_open_server(get_comm_port());
	if(sockfd < 0)
	{
		LOGE("[%s:%d:%s] Retrying connection in 5 seconds...\n",__FILE__,__LINE__,__func__);
		sleep(5);
		goto retry_connect;
	}
	
	//Create a message queue
	g_qinfo = msg_queue_create();
	if( g_qinfo == NULL )
	{	
		return EXIT_FAILURE;
	}

	//Start receive loop thread
	g_recv_loop_control = 1;		
	if(0 != pthread_create(&recv_tid,NULL,recv_loop,&sockfd) )
	{
		LOGE("[%s:%d:%s] pthread_create error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
		return EXIT_FAILURE;
	}
#if 0	
	//Start respond loop thread
	g_res_loop_control = 1;
	if(0 != pthread_create(&res_tid,NULL,respond_loop,&sockfd) )
	{
		LOGE("[%s:%d:%s] pthread_create error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
		return EXIT_FAILURE;
	}
	
	//Start uart loop.
	g_uart_loop_control =1;
	uart_loop(&sockfd);
#endif	
	//Start respond loop thread
	g_res_loop_control = 1;
	respond_loop(&sockfd);
	
	//If control reaches here, cleanup things.
	tcp_close_socket(sockfd);
	msg_queue_flush(g_qinfo);
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);
	return EXIT_SUCCESS;
}
