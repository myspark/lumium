var searchData=
[
  ['tail',['tail',['../structlist.html#a48b14941821b675251a99353ead6bf27',1,'list']]],
  ['tcp_5fclose_5fsocket',['tcp_close_socket',['../tcp__comm_8c.html#a1bab8bbe0ee89e4916a26304bbab522f',1,'tcp_close_socket(int sockfd):&#160;tcp_comm.c'],['../tcp__comm_8h.html#a1bab8bbe0ee89e4916a26304bbab522f',1,'tcp_close_socket(int sockfd):&#160;tcp_comm.c']]],
  ['tcp_5fcomm_2ec',['tcp_comm.c',['../tcp__comm_8c.html',1,'']]],
  ['tcp_5fcomm_2eh',['tcp_comm.h',['../tcp__comm_8h.html',1,'']]],
  ['tcp_5fopen_5fserver',['tcp_open_server',['../tcp__comm_8c.html#a059f46ab7ac3745682a46f2ed9c362f2',1,'tcp_open_server(int port):&#160;tcp_comm.c'],['../tcp__comm_8h.html#a059f46ab7ac3745682a46f2ed9c362f2',1,'tcp_open_server(int port):&#160;tcp_comm.c']]],
  ['tcp_5frecv_5fmsg',['tcp_recv_msg',['../tcp__comm_8c.html#afb975763f6e685a345a9fdc378d742b7',1,'tcp_recv_msg(int sockfd, char *data, int size, int timeout_ms):&#160;tcp_comm.c'],['../tcp__comm_8h.html#afb975763f6e685a345a9fdc378d742b7',1,'tcp_recv_msg(int sockfd, char *data, int size, int timeout_ms):&#160;tcp_comm.c']]],
  ['tcp_5fsend_5fmsg',['tcp_send_msg',['../tcp__comm_8c.html#aa79b56181e59f4f4751f9ac8c77b503c',1,'tcp_send_msg(int sockfd, char *data, int size):&#160;tcp_comm.c'],['../tcp__comm_8h.html#aa79b56181e59f4f4751f9ac8c77b503c',1,'tcp_send_msg(int sockfd, char *data, int size):&#160;tcp_comm.c']]]
];
