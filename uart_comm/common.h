/**
 * @file	: common.h
 * @date	: 2013-11-07
 * @author	: Pratik Rathod
 * @brief	: Common header file for uartd program, contains configuration related
 *			  macros.
 **/

#ifndef H_COMMON
#define H_COMMON

//! default uart port, if not supplied via command line
#define DEFAULT_UART 	"/dev/ttyO2"
//! baud rate
#define BAUD  		 	B19200
//! various log levels.
#define LOGD(...) 	 printf(__VA_ARGS__)
#define LOGE(...) 	 printf(__VA_ARGS__)
//#define LOGV(...)	 printf(__VA_ARGS__)
#define LOGV(...)	 (void)0
//#define LOGF(...)	 printf(__VA_ARGS__)
#define LOGF(...)	 (void)0

//! FIFO names prefixes.
#define FIFO_PREFIX  "/tmp/uartd_"

#endif
