/**
 * @file	: usb_camera_control.h
 * @date	: 2013-12-03
 * @author	: Pratik Rathod
 * @brief	: macro and enums related to usb camera control
 *
 */

#ifndef H_USB_CAMERA_CONTROL
#define H_USB_CAMERA_CONTROL

//!  video device to use for usb web camera
#define VIDEO_DEV			"/dev/video0"

//! enums for video streaming control
typedef enum _stream_cmd{
	STREAM_START=0, //! start video streaming
	STREAM_STOP //! stop video streaming
}stream_cmd;

/**
 * @brief : Start or Stop video stream over tcp network on specified ip and port.
 * @param	: operation [in] Start or Stop operation
 * @param	: ip [in] Ip address
 * @param	: port [in] port no. 
  * @param	: video_dev [in] video device to use i.e. /dev/videoX
 * @returns : On successful operation returns EXIT_SUCCESS, else EXIT_FAILURE
 */
int usb_camera_vid_stream(stream_cmd operation, char *ip, int port, char *video_dev);

/**
 * @brief : Enables/Disables video on LCD using network stream port no.
 * @param	: operation [in] Start or Stop operation
 * @param	: port [in] port no. to use
 * @param	: <param1> [<in/out>] <description> 
 * @returns : On successful operation, returns EXIT_SUCCESS else EXIT_FAILURE
 */
int usb_camera_vid_lcd(stream_cmd operation, int port);

#endif
