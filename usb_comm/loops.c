/**
 * @file	: loops.c
 * @date	: 2013-11-12
 * @author	: Pratik Rathod
 * @brief	: functions defined in this file are responsible to be used
 *			  as loop and running them in thread. It contains tcp recv & respond,
 *			  uart loop functions.
 **/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include "common.h"
#include "msg_queue.h"
#include "tcp_comm.h"
#include "command.h"
#include "loops.h"
#include "config.h"

//! external global queue info pointer.
extern queue_info *g_qinfo;
//!control flag for tcp recv loop
int g_recv_loop_control = 0;
//!control flag for tcp response loop
int g_res_loop_control = 0;
//!control flag for uart loop
int g_uart_loop_control = 0;

/**
 * @brief : Infinite loop to receive the data from TCP server application and add
 *          them to fifo queue for response loop to take care of it.
 * @param	: fd [in] pointer of tcp connected socket file descriptor
 */
void *recv_loop(void *fd)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int sockfd = *((int *)fd);
	char data[MAX_DATA];
	int rc;

	while(g_recv_loop_control)
	{
		//Receive server message with timeout of RECV_TIMEOUT_MS
		rc= tcp_recv_msg(sockfd,data,sizeof data, RECV_TIMEOUT_MS);
		if( rc > 0 )
		{
			// add message to fifo queue.
			if( EXIT_SUCCESS != msg_queue_add_data(g_qinfo,data,rc) )
			{
				LOGE("[%s:%d:%s] failed to add element in queue\n",__FILE__,__LINE__,__func__);
			}
		}
		else if ( rc < 0 ) {
			// connection closed.shut down own and others.
			g_recv_loop_control = 0;
			g_recv_loop_control = 0;
			g_uart_loop_control = 0;
		}		
	}
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);	
	return NULL;
}

/**
 * @brief : Infinite loop function to respond on TCP servers' messages. It reads 
 *           the head of queue and performs according action.
 * @param	: *fd [in] pointer to tcp connected socket file descriptor
 */
void *respond_loop(void *fd)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int rc = 0;
	int sockfd = *((int *)fd);
	char uart_write_fifo[256]; 
	int uart_write_fd;
	int write_bytes;
	char data[MAX_DATA];

/*****************************************************************************
 * retry the opening of uart fifo. They will be created by uartd process.
 * write uart messages from tcp server to uart write fifo, so uartd process
 * can write them on actual uart.
 *****************************************************************************/
retry_uart:	
	//strcpy(uart_write_fifo,UART_1_FIFO_PREFIX);
	get_uart_prefix(uart_write_fifo,sizeof uart_write_fifo);
	strcat(uart_write_fifo,"write");
	
	uart_write_fd = open(uart_write_fifo,O_RDWR);
	if(uart_write_fd < 0)
	{
		LOGE("[%s:%d:%s] open failed %s : [%s]\n",__FILE__,__LINE__,__func__,uart_write_fifo,strerror(errno));
		sleep(3);
		goto retry_uart;		
	}
	
	while(g_recv_loop_control)
	{
		rc = msg_queue_read_head(g_qinfo,data,sizeof data);
		if( rc > 0  )
		{
			cmd c;
			memset(&c,0,sizeof c);
			/*
			 * NO_ERROR --> check for command and perfrom according action
			 * INVALID_CHECKSUM --> Send NACK to Host application
			 * Othere --> do nothing.
			 */
			switch( command_parse(&c,data,rc) )
			{
				case NO_ERROR:
					if(EXIT_SUCCESS != command_action(c,sockfd))
					{
						if(uart_write_fd>0)
						{
							write_bytes = write(uart_write_fd,data,rc);
							if(write_bytes != rc )
							{
								LOGE("[%s:%d:%s] write error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
								data[1] = 0x05;
								data[3] = NACK;
								data[4] = command_xor_chksm(data,4);
								tcp_send_msg(sockfd,data,5);
							}
							else
							{
								LOGE("[%s:%d:%s] UART not responded, sending default NACK\n",__FILE__,__LINE__,__func__);
								/*if uart does not respond within timeout, send NACK to Host.*/
								if ( uart_operation(sockfd) != EXIT_SUCCESS )
								{
									data[1] = 0x05;
									data[3] = NACK;
									data[4] = command_xor_chksm(data,4);
									tcp_send_msg(sockfd,data,5);
								}						
							}
						}
					}
					break;
					
				case INVALID_CHECKSUM:
						data[1] = 0x05;
						data[3] = NACK;
						data[4] = command_xor_chksm(data,4);
						tcp_send_msg(sockfd,data,5);
						break;
						
				case DATA_OUT_OF_BOUND:
						break;
				case UNKNOWN_ERROR:
						break;																	
			}
			msg_queue_del_head(g_qinfo);
			//sleep(3);
		}
		usleep(500000);
	}
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);	
	return NULL;
}

/**
 * @brief : Infinite loop function to monitor uart read fifos for incoming uart data
 *          and pass it to TCP server
 * @param	: *fd [in] pointer to connected TCP socket descriptor
 */
void *uart_loop(void *fd)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int sockfd = *((int *)fd);
	int i,j,read_bytes;
	char uart_read_fifo[256];
	int uart_read_fd[MAX_UART];
	struct pollfd fds[MAX_UART];
	char data[MAX_DATA];

/*****************************************************************************
 * retry the opening of uart read fifo. They will be created by uartd process.
 * write messages from uart to TCP server fifo.
 *****************************************************************************/
retry_fifo:	
	memset(uart_read_fifo,0,sizeof uart_read_fifo);
	//strcpy(uart_read_fifo,UART_1_FIFO_PREFIX);
	get_uart_prefix(uart_read_fifo,sizeof uart_read_fifo);
	strcat(uart_read_fifo,"read");
	
	uart_read_fd[0] = open(uart_read_fifo, O_RDONLY | O_NONBLOCK);
	if(uart_read_fd[0] < 0)
	{
		LOGE("[%s:%d:%s] open failed for %s : [%s]\n",__FILE__,__LINE__,__func__,uart_read_fifo, strerror(errno));
		sleep(3);
		goto retry_fifo;
	}
	fcntl(uart_read_fd[0], F_SETFL, 0);
	
	memset(fds,0,sizeof fds);
	
	for(i=0;i<MAX_UART;i++)
	{
		fds[i].fd = uart_read_fd[i];
		fds[i].events = POLLIN;
	}
	
	while(g_uart_loop_control)
	{
		i = poll(fds,1,UART_TIMEOUT_MS);
		if( i != 0 )
		{
			for(j=0;j<MAX_UART;j++)
			{
				if( (fds[j].revents & POLLIN ) != 0 )
				{
					memset(data,0,sizeof data);		
					read_bytes = read(uart_read_fd[j],data,sizeof data);
					if(read_bytes>0)
					{
						LOGD("[%s:%d:%s] UART[%d] SAYS : [%s]\n",__FILE__,__LINE__,__func__,j,data);
						tcp_send_msg(sockfd,data,strlen(data));
					}
				}			
			}
		}
		else
		{
			LOGV("[%s:%d:%s] pipe poll timeout\n",__FILE__,__LINE__,__func__);
		}
	}
	
	//close pipes
	for(i=0;i<MAX_UART;i++)
	{
		if(uart_read_fd[i]>0)
			close(uart_read_fd[i]);	
	}


	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);
	return NULL;
}

/**
 * @brief : reads the uart response from read pipe with timeout of UART_TIMEOUT_MS
 *          if any data available then it directly sends over network
 * @param	: sockfd [in] socket fd of TCP communication,
 * @param	: <param1> [<in/out>] <description>
 * @param	: <param1> [<in/out>] <description> 
 * @returns : On successful operation returns EXIT_SUCCESS, else in case of timeout or
 *            any other error returns EXIT_FAILURE
 */
int uart_operation(int sockfd)
{
	int i,read_bytes;
	char uart_read_fifo[256];
	int uart_read_fd;
	struct pollfd fds;
	char data[MAX_DATA];
	int rc = EXIT_FAILURE;

	memset(uart_read_fifo,0,sizeof uart_read_fifo);
	//strcpy(uart_read_fifo,UART_1_FIFO_PREFIX);
	get_uart_prefix(uart_read_fifo,sizeof uart_read_fifo);	
	strcat(uart_read_fifo,"read");
	
	uart_read_fd = open(uart_read_fifo, O_RDONLY | O_NONBLOCK);
	if(uart_read_fd < 0)
	{
		LOGE("[%s:%d:%s] open failed for %s : [%s]\n",__FILE__,__LINE__,__func__,uart_read_fifo, strerror(errno));
	}
	else
	{
		fcntl(uart_read_fd, F_SETFL, 0);		

		memset(&fds,0,sizeof fds);
		fds.fd = uart_read_fd;
		fds.events = POLLIN;
		/*poll with timeout of UART_TIMEOUT_MS*/
		i = poll(&fds,1,UART_TIMEOUT_MS);
		if( i != 0 )
		{
			if( (fds.revents & POLLIN ) != 0 )
			{
				memset(data,0,sizeof data);		
				read_bytes = read(uart_read_fd,data,sizeof data);
				if(read_bytes>0)
				{
					LOGD("[%s:%d:%s] UART responce [%s]\n",__FILE__,__LINE__,__func__,data);
					tcp_send_msg(sockfd,data,strlen(data));
					rc = EXIT_SUCCESS;
				}
				else
				{
					LOGE("[%s:%d:%s] read error : %s",__FILE__,__LINE__,__func__,strerror(errno));
				}			
			}
			else
			{
				LOGE("[%s:%d:%s] not a POLLIN event",__FILE__,__LINE__,__func__);
			}						
		}
		else
		{
			LOGV("[%s:%d:%s] pipe poll timeout\n",__FILE__,__LINE__,__func__);
		}	
	}	
	return rc;
}
