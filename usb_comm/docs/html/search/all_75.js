var searchData=
[
  ['uart_5floop',['uart_loop',['../loops_8c.html#a05c10d0d926b6a4d81b2b182d08c82f1',1,'uart_loop(void *fd):&#160;loops.c'],['../loops_8h.html#a05c10d0d926b6a4d81b2b182d08c82f1',1,'uart_loop(void *fd):&#160;loops.c']]],
  ['uart_5foperation',['uart_operation',['../loops_8c.html#a3760e3e84057e94c4bb03659e439b2e3',1,'uart_operation(int sockfd):&#160;loops.c'],['../loops_8h.html#a3760e3e84057e94c4bb03659e439b2e3',1,'uart_operation(int sockfd):&#160;loops.c']]],
  ['uart_5fprefix',['uart_prefix',['../struct__config.html#a99246855cbae47e179bb951088db443a',1,'_config']]],
  ['uart_5ftimeout_5fms',['uart_timeout_ms',['../struct__config.html#a4d0df31d1b0e293ca85b42ba7933b01a',1,'_config']]],
  ['upload_5fgem_5fprint_5fimage',['UPLOAD_GEM_PRINT_IMAGE',['../command_8h.html#a033f2c2ba101d1649bd36de7783782f0a409ae692e55aaca80001269c20f0e968',1,'command.h']]],
  ['uvc',['UVC',['../camera__interface_8h.html#a3b5f92e1ab71ddb3554796fe2bb546d0a1354765731ec2c4cbd0fa80a9aba76de',1,'camera_interface.h']]]
];
