/**
 * @file	: main.c
 * @date	: 07 - 11 -2013
 * @author  : Pratik Rathod
 * @brief	: Functions defined here are responsible for starting the program,
 * 			  doing initialization and run the process.
 * 
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <sys/stat.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/types.h>
#include <libgen.h>
#include "common.h"	

//! named pipe path to read the uart data
char g_read_fifo_path[256];
//! named pipe path to write data to uart
char g_write_fifo_path[256];
//! function prototype
void *write_loop( void *t_fd );
void *read_loop( void *t_fd );
int uart_open( char *name );
int uart_read(int fd, char *data,int size,int timeout_ms);
int uart_write(int fd, char *buffer, int len);
int uart_close(int fd);

/**
 * @brief : Basic initialization, removing old fifos' and creating new ones
 * @returns : EXIT_SUCCESS on successful operation, else EXIT_FAILURE
 */
int init()
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	/* remove old fifos'  */
	unlink(g_read_fifo_path);
	unlink(g_write_fifo_path);
	
	/*****************************************************************************
	 * creating fifo for writing data to uart
	 * uartd will monitor this file. External processes can use this file
	 * in order to write data to uart.
	 *****************************************************************************/
	if ( 0 != mkfifo(g_write_fifo_path,S_IRWXU | S_IRWXG | S_IRWXO) )
	{
		LOGE("[%s:%d:%s] Can not create fifo %s : [%s]\n",__FILE__,__LINE__,__func__,g_write_fifo_path,strerror(errno));
		return EXIT_FAILURE;
	}

	/*****************************************************************************
	 * uartd will write the data read from uart to this fifo. External processes 
	 * can use this fifo in order to get data from uart.
	 *****************************************************************************/	
	if ( 0 != mkfifo(g_read_fifo_path,0644) )
	{
		LOGE("[%s:%d:%s] Can not create fifo %s : [%s]\n",__FILE__,__LINE__,__func__,g_read_fifo_path,strerror(errno));
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

/**
 * @brief : convert process in to daemon.
 */
void daemonize()
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	pid_t process_id = 0;
	pid_t sid = 0;
	
	// Create child process
	process_id = fork();
	// Indication of fork() failure
	if (process_id < 0)
	{
		LOGE("[%s:%d:%s] fork failed!\n",__FILE__,__LINE__,__func__);
		// Return failure in exit status
		exit(1);
	}
	
	// PARENT PROCESS. Need to kill it.
	if (process_id > 0)
	{
		LOGD("[%s:%d:%s] process_id of child process %d \n",__FILE__,__LINE__,__func__, process_id);
		// return success in exit status
		exit(0);
	}
	
	//unmask the file mode
	umask(0);
	//set new session
	sid = setsid();
	if(sid < 0)
	{
		// Return failure
		exit(1);
	}
	// Change the current working directory to root.
	chdir("/");
	// Close stdin. stdout and stderr
	close(0);
	close(1);
	close(2);	
}

/**
 * @brief : main function of the program, the entry point
 * @param	: argc [in] no. of arguments
 * @param	: argv [in] arguments
 * @param	: <param1> [<in/out>] <description> 
 * @returns : EXIT_SUCCESS on successful operation, else EXIT_FAILURE
 */
int main(int argc , char *argv[])
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	char uart_port[256];
	char *bname;
	int fd;
	pthread_t tid;

	//this call will exit the program if fails
//	daemonize();
	
	/*****************************************************************************
	 * Pass uart device path as command line argument o/w it will use
	 * DEFAULT_UART
	 *****************************************************************************/
	if(argc>1)
	{
		strncpy(uart_port,argv[1],sizeof uart_port);
	}
	else
	{
		strcpy(uart_port,DEFAULT_UART);
	}
	
	/*****************************************************************************
	 * fifo file naming convention:
	 * /tmp/uartd_<uart_device_name>_read - for reading
	 * /tmp/uartd_<uart_device_name>_write - for writing
	 * for example, using /dev/ttyO0
	 * /tmp/uartd_ttyO0_read - for read &
	 * /tmp/uartd_ttyO0_write - for write
	 *****************************************************************************/
	bname = (char *)basename(uart_port);
	if(bname != NULL)
	{
		strcpy(g_write_fifo_path,FIFO_PREFIX);
		strcat(g_write_fifo_path,bname);
		strcat(g_write_fifo_path,"_write");
	
		strcpy(g_read_fifo_path,FIFO_PREFIX);
		strcat(g_read_fifo_path,bname);
		strcat(g_read_fifo_path,"_read");		
	}
	else
	{
		LOGE("[%s:%d:%s] Enter a valid uart port path\n",__FILE__,__LINE__,__func__);
		return EXIT_FAILURE;
	}

	//opening uart device	
	fd = uart_open( uart_port );
	if(fd <= 0 )
	{
		return EXIT_FAILURE;
	}
	
	if ( init() != EXIT_SUCCESS )
	{
		return EXIT_FAILURE;
	}

	//starting write loop
	if (pthread_create(&tid,NULL,write_loop,&fd) != 0 )
	{
		LOGE("[%s:%d:%s] pthread_create failed : %s\n",__FILE__,__LINE__,__func__,strerror(errno));
		return EXIT_FAILURE;
	}
	//starting read loop
	read_loop(&fd);
	LOGE("[%s:%d:%s] I should not die... some thing is wrong.\n",__FILE__,__LINE__,__func__);
	uart_close(fd);
	return EXIT_SUCCESS;
}
