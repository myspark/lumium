var searchData=
[
  ['camera_5fget_5fdevice',['camera_get_device',['../camera__interface_8c.html#a136a7e254cb91ad25b71df34c71c39b1',1,'camera_get_device(char *video_dev, int size, camera_type cam):&#160;camera_interface.c'],['../camera__interface_8h.html#a136a7e254cb91ad25b71df34c71c39b1',1,'camera_get_device(char *video_dev, int size, camera_type cam):&#160;camera_interface.c']]],
  ['camera_5finterface_2ec',['camera_interface.c',['../camera__interface_8c.html',1,'']]],
  ['camera_5finterface_2eh',['camera_interface.h',['../camera__interface_8h.html',1,'']]],
  ['camera_5fstream',['camera_stream',['../camera__interface_8c.html#a6fa716bf6c1c2ab8bba2da52def9189c',1,'camera_stream(stream_cmd operation, char *video_dev, int port, camera_type cam):&#160;camera_interface.c'],['../camera__interface_8h.html#a6fa716bf6c1c2ab8bba2da52def9189c',1,'camera_stream(stream_cmd operation, char *video_dev, int port, camera_type cam):&#160;camera_interface.c']]],
  ['camera_5ftype',['camera_type',['../camera__interface_8h.html#a3b5f92e1ab71ddb3554796fe2bb546d0',1,'camera_interface.h']]],
  ['camera_5fvid_5flcd',['camera_vid_lcd',['../camera__interface_8c.html#ab1ba6f76075b3984f16b332c4c69f4f5',1,'camera_vid_lcd(stream_cmd operation, int port):&#160;camera_interface.c'],['../camera__interface_8h.html#ab1ba6f76075b3984f16b332c4c69f4f5',1,'camera_vid_lcd(stream_cmd operation, int port):&#160;camera_interface.c']]],
  ['chksum',['chksum',['../structmessage.html#a903289f55eb327a0308f1412fdbd8536',1,'message']]],
  ['cmd',['cmd',['../command_8h.html#ad7ab42f7e1d8a9d6d2056c7e8466d062',1,'command.h']]],
  ['cmd_5ftype',['cmd_type',['../command_8h.html#a13c71f7b23e929cf16f91fa86c93f7cf',1,'command.h']]],
  ['command',['command',['../structmessage.html#a80d9e6f7aec27095120ee37335dbd5fb',1,'message']]],
  ['command_2ec',['command.c',['../command_8c.html',1,'']]],
  ['command_2eh',['command.h',['../command_8h.html',1,'']]],
  ['command_5faction',['command_action',['../command_8c.html#a380a927321b577fa5a5e8c766aaa641a',1,'command_action(cmd c, int sockfd):&#160;command.c'],['../command_8h.html#a380a927321b577fa5a5e8c766aaa641a',1,'command_action(cmd c, int sockfd):&#160;command.c']]],
  ['command_5fparse',['command_parse',['../command_8c.html#abed180ca0d9c2adfad4a5031564c8667',1,'command_parse(cmd *c, const char *data, int size):&#160;command.c'],['../command_8h.html#abed180ca0d9c2adfad4a5031564c8667',1,'command_parse(cmd *c, const char *data, int size):&#160;command.c']]],
  ['command_5fxor_5fchksm',['command_xor_chksm',['../command_8c.html#a301c6fbbe71965f1a10a8a581c9aa647',1,'command_xor_chksm(const char *data, int size):&#160;command.c'],['../command_8h.html#a301c6fbbe71965f1a10a8a581c9aa647',1,'command_xor_chksm(const char *data, int size):&#160;command.c']]],
  ['commands',['commands',['../command_8h.html#a033f2c2ba101d1649bd36de7783782f0',1,'command.h']]],
  ['config',['config',['../config_8h.html#aa75d2304f2d943e86dadbe603a750c9d',1,'config.h']]],
  ['config_2ec',['config.c',['../config_8c.html',1,'']]],
  ['config_2eh',['config.h',['../config_8h.html',1,'']]],
  ['config_5ffile',['CONFIG_FILE',['../config_8c.html#a42bf63540ee0039a3a23db1b5ec3202b',1,'config.c']]],
  ['config_5finit',['config_init',['../config_8c.html#aa1d3b38b1da7f1a7f57c4a583ad1fdb1',1,'config_init():&#160;config.c'],['../config_8h.html#aa1d3b38b1da7f1a7f57c4a583ad1fdb1',1,'config_init():&#160;config.c']]]
];
