/**
 * @file	: config.c
 * @date	: 2013-12-31
 * @author	: Pratik Rathod
 * @brief	: functions related to configuration loading
 **/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include "config.h"
#include "common.h"

//! config file path
#define CONFIG_FILE    "/etc/usb_tcp.conf"

//! global structure to load config in to
config g_config;

/**
 * @brief : Reads config file and fills up the config structer
 */
void config_init()
{
	FILE *fp = NULL;

	/*default values*/
	g_config.comm_port = 9876;
	g_config.stream_port = 1234;
	g_config.uart_timeout_ms = 5000;
	strcpy(g_config.uart_prefix,UART_1_FIFO_PREFIX);
	fp = fopen(CONFIG_FILE,"r");
	if ( fp != NULL )
	{
		int read_bytes = 0;
		char *buffer = NULL;
		size_t len=0;
		while( ( read_bytes = getline(&buffer,&len,fp) ) != -1 )
		{
			LOGD("[%s:%s:%d] read : %s",__FILE__,__func__,__LINE__,buffer);
			if( strstr(buffer,"COMM_PORT:") != NULL )
			{
				sscanf(buffer,"COMM_PORT:%d",&g_config.comm_port);
			}
			else if( strstr(buffer,"STREAM_PORT:") != NULL )
			{
				sscanf(buffer,"STREAM_PORT:%d",&g_config.stream_port);			
			}
			else if( strstr(buffer,"UART_PREFIX:") != NULL )
			{
				sscanf(buffer,"UART_PREFIX:%s",g_config.uart_prefix);							
			}
			else if( strstr(buffer,"UART_TIMEOUT_MS:") != NULL )
			{
				sscanf(buffer,"UART_TIMEOUT_MS:%d",&g_config.uart_timeout_ms);							
			}			
		}		
		free(buffer);
		fclose(fp);
	}
	else
	{
		LOGE("[%s:%s:%d] fopen failed for %s : %s\n",__FILE__,__func__,__LINE__,CONFIG_FILE,strerror(errno));
	}
}

/**
 * @brief : getter function for communication port numner
 * @returns : communication port number
 */
int get_comm_port()
{
	return g_config.comm_port;
}

/**
 * @brief : getter function for video streaming port
 * @returns : video streaming port
 */
int get_stream_port()
{
	return g_config.stream_port;
}

/**
 * @brief : getter function for uart prefix
 * @param	: buffer [ou] buffer to write uart prefix
 * @param	: size [in] size of buffer
 */
void get_uart_prefix(char *buffer,int size)
{
	memset(buffer,0,size);
	strncpy(buffer,g_config.uart_prefix,size);
}
#if 0
void print_configs()
{
	printf("comm port : %d\n",g_config.comm_port);
	printf("uart port : %d\n",g_config.stream_port);
	printf("uart prefix : %s\n",g_config.uart_prefix);		
}

int main()
{
	config_init();
	print_configs();
}
#endif
