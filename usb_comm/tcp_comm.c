/**
 * @file	: tcp_comm.c
 * @date	: 2013-11-13
 * @author	: Pratik Rathod
 * @brief	: Functions defined here are responsible for creating/managing TCP
 *            client socket communication.
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <pthread.h>
#include "common.h"

//! mutex and its initialization for thread safe operations
pthread_mutex_t g_tcpmutex = PTHREAD_MUTEX_INITIALIZER;

/**
 * @brief : Opens and Starts TCP server using ip and port provided. This 
 *          call may block until client is not connected to server.
 * @param	: ip [in] IP address of the TCP server.
 * @param	: port [in] port number of the TCP server.
 * @param	: <param1> [<in/out>] <description> 
 * @returns : On successful operation returns socket fd of connected socket. else -1
 */
int tcp_open_server(int port)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	struct sockaddr_in servaddr,cliaddr;
	int sockfd;
	int clientfd = -1;
	socklen_t clilen;
	
	//create TCP socket
	sockfd=socket(AF_INET,SOCK_STREAM,0);
	if( sockfd <= 0 )
	{
		LOGE("[%s:%d:%s] socket error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
	}
	else
	{
		memset(&servaddr,0,sizeof(servaddr));
		servaddr.sin_family = AF_INET;
		servaddr.sin_addr.s_addr=htonl(INADDR_ANY);
		servaddr.sin_port=htons(port);
		if ( bind(sockfd,(struct sockaddr *)&servaddr,sizeof(servaddr)) == 0 ) 
		{
			if ( listen(sockfd, 1) == 0 )
			{
				clilen=sizeof(cliaddr);
				clientfd = accept(sockfd,(struct sockaddr *)&cliaddr,&clilen);
				if( clientfd <= 0 )
				{
					LOGE("[%s:%d:%s] accept error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
					clientfd = -1;
				}
			}
			else
			{
				LOGE("[%s:%d:%s] listen error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));			
			}
		}
		else
		{
			LOGE("[%s:%d:%s] bind error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
		}
	}
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);	
	return clientfd;
}

/**
 * @brief : Receive message using provided socket fd.
 * @param	: sockfd [in] file descriptor of connected socket.
 * @param	: data [out] data buffer to store received data
 * @param	: size [in] size of data buffer 
 * @param	: timeout_ms [in] Time out in milli seconds.
 * @returns : return no. of bytes read. On timeout returns 0. On error returns -1
 */
int tcp_recv_msg(int sockfd, char *data, int size, int timeout_ms)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int recv_bytes=-1,i,j;
	struct pollfd fds;
	
	memset(&fds,0,sizeof fds);
	memset(data,0,size);
	fds.fd = sockfd;
	fds.events = POLLIN | POLLHUP;
	
	i = poll(&fds,1,timeout_ms);
	if( i != 0 )
	{
		// data available to read
		if( (fds.revents & POLLIN ) != 0 )
		{
			recv_bytes=recv(sockfd,data,size,0);
			if( recv_bytes > 0 )
			{
				LOGD("[%s:%d:%s] received : [%s]\n",__FILE__,__LINE__,__func__,data);
				for(j=0;j<recv_bytes;j++)
					LOGV("[%s:%d:%s] [%d] : {%x} \n",__FILE__,__LINE__,__func__,j,data[j]);
			}
			else if ( recv_bytes == 0 )
			{
				LOGE("[%s:%d:%s] connection closed : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
				recv_bytes=-1;
			}
			else
			{
				LOGE("[%s:%d:%s] recv error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
			}
		}
		// connection closed.
		if( (fds.revents & POLLHUP ) != 0 )
		{
			LOGE("[%s:%d:%s] POLLHUP : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));		
		}
	}
	else
	{
		LOGV("[%s:%d:%s] poll timeout fd[%d]\n",__FILE__,__LINE__,__func__,sockfd);
		recv_bytes=0;
	}
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);
	return recv_bytes;
}

/**
 * @brief : Send message using provided socket fd
 * @param	: sockfd [in] file descriptor of connected socket.
 * @param	: data [in] data to send as message.
 * @param	: size [in] size of data buffer 
 * @returns : On successful operation returns no. of bytes written. On error -1.
 */
int tcp_send_msg(int sockfd, char *data, int size)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int write_bytes=-1;
	
	MUTEX_LOCK(&g_tcpmutex);
	write_bytes = send(sockfd,data,size,0);
	if ( write_bytes != size ) 
	{
		LOGE("[%s:%d:%s] write error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
		write_bytes = -1;
	}
	MUTEX_UNLOCK(&g_tcpmutex);
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);	
	return write_bytes;
}

/**
 * @brief : Closes the provided fd
 * @param	: sockfd [in] file descriptor of connected socket.
 */
void tcp_close_socket(int sockfd)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	if(sockfd>0)
	{
		close(sockfd);
		LOGD("[%s:%d:%s] socket closed\n",__FILE__,__LINE__,__func__);
	}
	else
	{
		LOGD("[%s:%d:%s] not a valid socket fd\n",__FILE__,__LINE__,__func__);
	}
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);	
}
