#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <termios.h>


#define CONFIG_FILE "/etc/uartd_config"

typedef struct _config {
    char port[64];
    speed_t baud;
    int timeout_ms;
} configs;

configs g_configs;

int config_init()
{
	FILE *fp = NULL;
	int rc = EXIT_FAILURE;
	char buffer[512];
	
	memset(&g_configs,0,sizeof g_configs);	
	memset(buffer,0,sizeof buffer);
	fp = fopen(CONFIG_FILE,"r");
	if(fp != NULL)
	{
		while( fscanf(fp,"%s",buffer) > 0 )
		{
			if( strstr(buffer,"PORT:") != NULL )
			{
				fscanf(buffer,"PORT:%s",g_configs.port);
			} 
			else if ( strstr(buffer,"BAUD:") != NULL )
			{
				fscanf(buffer,"BAUD:%d",g_configs.port);			
			}
		}
	}
	else
	{
		/*load default configs*/
		strcpy(g_configs.port,DEFAULT_UART);
		g_configs.baud = BAUD;
		g_configs.timeout_ms = 5000;
	}
}
