/**
 * @file	: config.h
 * @date	: 2013-12-31
 * @author	: Pratik Rathod
 * @brief	: structures, macros, enums for loading configurations 
 *
 */

#ifndef H_CONFIG
#define H_CONFIG

//! configuration parameters
typedef struct _config{
	int comm_port; //! TCP communication port with HOST PC
	int stream_port; //! TCP video streaming port
	int uart_timeout_ms; //! UART responce timeout
	char uart_prefix[256]; //! uart fifo prefix to use
} config;

/**
 * @brief : Reads config file and fills up the config structer
 */
void config_init();

/**
 * @brief : getter function for communication port numner
 * @returns : communication port number
 */
int get_comm_port();

/**
 * @brief : getter function for video streaming port
 * @returns : video streaming port
 */
int get_stream_port();

/**
 * @brief : getter function for uart prefix
 * @param	: buffer [ou] buffer to write uart prefix
 * @param	: size [in] size of buffer
 */
void get_uart_prefix(char *buffer,int size);

#endif
