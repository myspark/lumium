/**
 * @file	: tcp_comm.h
 * @date	: 2013-11-13
 * @author	: Pratik Rathod
 * @brief	: Function declarations for tcp client communication.
 **/

#ifndef H_TCP_COMM
#define H_TCP_COMM

/**
 * @brief : Opens and Starts TCP server using ip and port provided. This 
 *          call may block until client is not connected to server.
 * @param	: port [in] port number of the TCP server.
 * @returns : On successful operation returns socket fd of connected socket. else -1
 */
int tcp_open_server(int port);

/**
 * @brief : Receive message using provided socket fd.
 * @param	: sockfd [in] file descriptor of connected socket.
 * @param	: data [out] data buffer to store received data
 * @param	: size [in] size of data buffer 
 * @param	: timeout_ms [in] Time out in milli seconds.
 * @returns : return no. of bytes read. On timeout returns 0. On error returns -1
 */
int tcp_recv_msg(int sockfd, char *data, int size, int timeout_ms);

/**
 * @brief : Send message using provided socket fd
 * @param	: sockfd [in] file descriptor of connected socket.
 * @param	: data [in] data to send as message.
 * @param	: size [in] size of data buffer 
 * @returns : On successful operation returns no. of bytes written. On error -1.
 */
int tcp_send_msg(int sockfd, char *data, int size);

/**
 * @brief : Closes the provided fd
 * @param	: sockfd [in] file descriptor of connected socket.
 */
void tcp_close_socket(int sockfd);

#endif
