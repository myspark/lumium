/**
 * @file	: loops.c
 * @date	: 2013-11-07
 * @author	: Pratik Rathod
 * @brief	: Functions defined in this file are responsible for polling uart port
 *            for writing the data, reading the data and do redirection from fifo
 *            for external process interfaces.
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <poll.h>
#include <errno.h>
#include "common.h"

//! extern global variable
extern char g_read_fifo_path[256];
//! extern global variable
extern char g_write_fifo_path[256];

//! function prototypes
int uart_read(int fd, char *data,int size,int timeout_ms);
int uart_write(int fd, char *buffer, int len);

/**
 * @brief : read from uart device, this function will loop continuously and does its job
 *          Can be used as thread. This function will look for data on uart port and  if data
 *          available in uart , it will redirect the data to "read fifo"
 * @param	: *fd [in] pointer to file descriptor of uart
 */
void *read_loop( void *t_fd )
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int fd = *((int *)t_fd);
	int timeout_ms = 3000; //! timeout for read
	char data[1024];
	int fifo_fd = 0;

	fifo_fd = open(g_read_fifo_path,O_RDWR);
	if(fifo_fd > 0)
	{
		int read_bytes=0;
		while(1)
		{
			read_bytes = uart_read( fd, data, sizeof data, timeout_ms);
			if( read_bytes > 0)
			{
				LOGD("[%s:%d:%s] UART says : [%s]\n",__FILE__,__LINE__,__func__,data);
				write(fifo_fd,data,read_bytes);
			}
		}		
		close(fifo_fd);
	}
	else
	{
		LOGE("[%s:%d:%s] Can not open fifo : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
	}
	
	return NULL;
}

/**
 * @brief : write to uart device, this function will loop continuously and does its job
 *          Can be used as thread. This function will read from "write fifo" and if data
 *          available in fifo , it will write it to uart port.
 * @param	: *fd [in] pointer to file descriptor of uart
 */
void *write_loop( void *t_fd )
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int fd = *((int *)t_fd);
	int fifo_fd=0;
	
	fifo_fd = open(g_write_fifo_path,O_RDONLY);
	if(fifo_fd > 0)
	{
		int read_bytes=0;
		char fifo_buffer[1024];
		
		while(1)
		{							
			memset(fifo_buffer,0,sizeof fifo_buffer);
			read_bytes = read(fifo_fd,fifo_buffer,sizeof fifo_buffer);
			if ( read_bytes > 0 )
			{
				LOGD("[%s:%d:%s] FIFO says : [%s]\n",__FILE__,__LINE__,__func__,fifo_buffer);
				uart_write( fd, fifo_buffer, read_bytes);
			}
			usleep(500000);
		}
		close(fifo_fd);
	}
	else
	{
		LOGE("[%s:%d:%s] Can not open fifo : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
	}
	
	return NULL;
}
