/**
 * @file	: camera_interface.c
 * @date	: 2013-12-31
 * @author	: Pratik Rathod
 * @brief	: Functions related to camera device detection/stream control
 **/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <dirent.h>
#include <gst/gst.h>
#include "camera_interface.h"
#include "common.h"

/**
 * @brief : gets the device node for video device type specified in argument cam
 * @param	: video_dev [out] buffer to write video device name
 * @param	: size [in] size of the input buffer
 * @param	: cam [in] type of camera, RCA or UVC 
 * @returns : On successful operation EXIT_SUCCESS, else EXIT_FAILURE
 */
int camera_get_device(char *video_dev,int size,camera_type cam)
{
	int rc = EXIT_FAILURE;
	struct dirent *dent;
	DIR *dir;
	char *dname = "/sys/class/video4linux/";
	int node_found=0;
	
	/* We'll traverse through video dirs in sys entry of video4linux and detect
	 * the type of camera by reading "name" entry of the detected camera
	 */
	dir = opendir(dname);
	if( dir != NULL )
	{
		while((dent = readdir(dir)))
		{
			/*It can be directory and link*/
			if( ( dent->d_type == DT_DIR ) || ( dent->d_type == DT_LNK ) )
			{
				LOGD("[%s:%s:%d] found : %s\n",__FILE__,__func__,__LINE__,dent->d_name);			
				if( strstr(dent->d_name,"video") != NULL )
				{
					char name_path[strlen(dname)+strlen(dent->d_name)+1];
					memset(name_path,0,sizeof name_path);
					strcpy(name_path,dname);
					strcat(name_path,dent->d_name);
					strcat(name_path,"/name");
					LOGD("[%s:%s:%d] Checking in file %s\n",__FILE__,__func__,__LINE__,name_path);
					FILE *fp = fopen(name_path,"r");
					if( fp != NULL )
					{
						char buffer[256];
						memset(buffer,0,sizeof buffer);
						fscanf(fp,"%256s",buffer);
						fclose(fp);
						LOGD("[%s:%s:%d] read string : %s\n",__FILE__,__func__,__LINE__,buffer);
						/*
						 * - If read value contains "tvp514x" string then it is RCA camera device node
						 * - If read value contains "UVC" string then it is UVC camera device connected
						 *   to usb port.
						 */
						if( cam == RCA )
						{
							if( strstr(buffer,"tvp514x") != NULL )
							{							
								strcpy(video_dev,"/dev/");
								strcat(video_dev,dent->d_name);
								node_found = 1;
								break;
							}
						} 
						else if ( cam == UVC )
						{
							if( strstr(buffer,"UVC") != NULL )
							{
								strcpy(video_dev,"/dev/");
								strcat(video_dev,dent->d_name);
								node_found = 1;
								break;
							}						
						}

					}
					else
					{
						LOGE("[%s:%s:%d] fopen failed for %s : %s\n",__FILE__,__func__,__LINE__,name_path,strerror(errno));
					}
				}
			}
		}
		closedir(dir);
		if(node_found)
		{
			LOGD("[%s:%s:%d] video node found : %s\n",__FILE__,__func__,__LINE__,video_dev);
			rc = EXIT_SUCCESS;
		}
	}
	else
	{
		LOGE("[%s:%s:%d] opendir failed for %s : %s\n",__FILE__,__func__,__LINE__,dname,strerror(errno));
	}
	return rc;
}

/**
 * @brief : Start or Stop video stream over tcp network on specified ip and port.
 * @param	: operation [in] Start or Stop operation
 * @param	: video_dev [in] video device to use i.e. /dev/videoX
 * @param	: port [in] port no. 
 * @param	: cam [in] Camera device to use, either RCA or UVC
 * @returns : On successful operation returns EXIT_SUCCESS, else EXIT_FAILURE
 */
int camera_stream(stream_cmd operation, char *video_dev, int port,camera_type cam)
{
	/*Keeping pipeline static varible to keep track of streaming status
	 * if it is NULL, than no streaming is going on
	 * if it is not NULL, than streaming is in progress.
	 * This will avoid usage of a global pipeline pointer variable.
	 */
	static GstElement *pipeline = NULL;
	GstStateChangeReturn ret;	/*we want the status change return from gst*/
	int rc = EXIT_FAILURE;
	
	switch(operation)
	{
		case STREAM_START:
			/*if pipeline is NULL, then we need to start streaming*/
			if( pipeline == NULL )
			{
				char cmd[1024];
				memset(cmd,0,sizeof cmd);
				/*generating pipeline string from supplied arguments
				 *this can be directly passed to gst_parse_launch
				 */
				 if( cam == RCA )
				 {
					sprintf(cmd,"v4l2src always-copy=FALSE input-src=composite queue-size=1 device=%s ! video/x-raw-yuv,format=(fourcc)UYVY,width=720,height=576 ! ffmpegcolorspace  ! jpegenc ! multipartmux ! tcpserversink port=%d", video_dev,port);				 
				 }
				 else if ( cam == UVC )
				 {
					sprintf(cmd,"v4l2src device=%s ! video/x-raw-yuv,framerate=(fraction)30/1 ! ffmpegcolorspace  ! jpegenc ! multipartmux ! tcpserversink port=%d", video_dev,port);				 
				 }


				LOGV("[%s:%s:%d] pipeline : %s",__FILE__,__func__,__LINE__,cmd);

				/*init gstremer, no arguments for gst*/
				gst_init(NULL,NULL);
				/*creating actual pipeline element.*/
				pipeline = gst_parse_launch(cmd,NULL);
				if(pipeline == NULL)					
				{
					LOGE("[%s:%s:%d] gst_parse_launch() failed.\n",__FILE__,__func__,__LINE__);
				}
				else
				{					
					LOGV("[%s:%s:%d] Seting state to playing",__FILE__,__func__,__LINE__);
					/*setting GST_STATE_PLAYING will start streaming.*/
					ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);				
					if ( ret == GST_STATE_CHANGE_FAILURE )
					{
						LOGE("[%s:%s:%d] gst_element_set_state() failed\n",__FILE__,__func__,__LINE__);
						GstMessage *msg=NULL; /*for getting message from gst if any*/
						GstBus *bus=NULL;							
						/*timeout of 1 sec to get message from bus if any*/
						GstClockTime timeount_ns = GST_CLOCK_TIME_NONE;
						/*any final words from pipeline??...*/
						bus = gst_element_get_bus(pipeline);
						msg = gst_bus_timed_pop_filtered (bus, timeount_ns, GST_MESSAGE_ERROR | GST_MESSAGE_EOS );
						if( msg != NULL )
						{
							GError *err;
							gchar *debug_info;
		
							switch(GST_MESSAGE_TYPE(msg))
							{
								case GST_MESSAGE_ERROR:
									gst_message_parse_error(msg, &err, &debug_info);
									LOGE("[%s:%s:%d] error recieved from element %s : %s\n",__FILE__,__func__,__LINE__,GST_OBJECT_NAME(msg->src),err->message);
									LOGE("[%s:%s:%d] Debug info : %s\n",__FILE__,__func__,__LINE__, debug_info ? debug_info : "none");
									g_clear_error(&err);
									g_free(debug_info);
									break;
			
								case GST_MESSAGE_EOS:
									LOGD("[%s:%s:%d] End-of-Stream reached\n",__FILE__,__func__,__LINE__);
									break;
			
								default:
									LOGE("[%s:%s:%d] Unexpected message recieved\n",__FILE__,__func__,__LINE__);
									break;							
							}
							gst_message_unref(msg);
						}
						gst_object_unref(bus);
						gst_object_unref(pipeline);
						 /*setting NULL to make sure we can start streaming next time*/					
						pipeline=NULL;							
					}
					else
					{
						LOGD("[%s:%s:%d] Streaming started\n",__FILE__,__func__,__LINE__);
						rc = EXIT_SUCCESS;
					}
				}							
			} //==> if pipline != NULL 
			else
			{
				LOGE("[%s:%s:%d] Streaming already in progress\n",__FILE__,__func__,__LINE__);
			} 
			break;	//==> case : STREAM_START
					
		case STREAM_STOP:
			/* 
			 * if pipeline is not NULL than streaming is in progress
			 * note the usage of static variable here
			 */
			if(pipeline != NULL)
			{
				GstMessage *msg=NULL; /*for getting message from gst if any*/
				GstBus *bus=NULL;
				/*GST_STATE_NULL will stop the stream*/			
				ret = gst_element_set_state(pipeline,GST_STATE_NULL);
				if( ret == GST_STATE_CHANGE_FAILURE )
				{
					LOGE("[%s:%s:%d] gst_element_set_state() failed for GST_STATE_NULL\n",__FILE__,__func__,__LINE__);
				}
				else
				{
					/*timeout of 1 sec to get message from bus if any*/
					GstClockTime timeount_ns = 1000000000;
					/*any final words from pipeline??...*/
					bus = gst_element_get_bus(pipeline);
					msg = gst_bus_timed_pop_filtered (bus, timeount_ns, GST_MESSAGE_ERROR | GST_MESSAGE_EOS );
					if( msg != NULL )
					{
						GError *err;
						gchar *debug_info;
			
						switch(GST_MESSAGE_TYPE(msg))
						{
							case GST_MESSAGE_ERROR:
								gst_message_parse_error(msg, &err, &debug_info);
								LOGE("[%s:%s:%d] error recieved from element %s : %s\n",__FILE__,__func__,__LINE__,GST_OBJECT_NAME(msg->src),err->message);
								LOGE("[%s:%s:%d] Debug info : %s\n",__FILE__,__func__,__LINE__, debug_info ? debug_info : "none");
								g_clear_error(&err);
								g_free(debug_info);
								break;
				
							case GST_MESSAGE_EOS:
								LOGD("[%s:%s:%d] End-of-Stream reached\n",__FILE__,__func__,__LINE__);
								break;
				
							default:
								LOGE("[%s:%s:%d] Unexpected message recieved\n",__FILE__,__func__,__LINE__);
								break;							
						}
						gst_message_unref(msg);
					}
					gst_object_unref(bus);
					gst_object_unref(pipeline);
					 /*setting NULL to make sure we can start streaming next time*/					
					pipeline=NULL;
					rc = EXIT_SUCCESS;
				}
			}
			else
			{
				LOGE("[%s:%s:%d] Streaming not started yet\n",__FILE__,__func__,__LINE__);
			}
			break; //==> case : STREAM_STOP
	}
	
	return rc;						
}
#if 0
int camera_uvc_stream(stream_cmd operation, char *video_dev, int port)
{
	/*Keeping pipeline static varible to keep track of streaming status
	 * if it is NULL, than no streaming is going on
	 * if it is not NULL, than streaming is in progress.
	 * This will avoid usage of a global pipeline pointer variable.
	 */
	static GstElement *pipeline = NULL;
	GstStateChangeReturn ret;	/*we want the status change return from gst*/
	int rc = EXIT_FAILURE;
	
	switch(operation)
	{
		case STREAM_START:
			/*if pipeline is NULL, then we need to start streaming*/
			if( pipeline == NULL )
			{
				char cmd[1024];
				memset(cmd,0,sizeof cmd);
				/*generating pipeline string from supplied arguments
				 *this can be directly passed to gst_parse_launch
				 */
				sprintf(cmd,"v4l2src device=%s ! video/x-raw-yuv,framerate=(fraction)30/1 ! ffmpegcolorspace  ! jpegenc ! multipartmux ! tcpserversink port=%d", video_dev,port);

				LOGD("pipeline : %s",cmd);

				/*init gstremer, no arguments for gst*/
				gst_init(NULL,NULL);
				/*creating actual pipeline element.*/
				pipeline = gst_parse_launch(cmd,NULL);
				if(pipeline == NULL)					
				{
					LOGE("gst_parse_launch() failed.\n");
				}
				else
				{					
					LOGV("Seting state to playing");
					/*setting GST_STATE_PLAYING will start streaming.*/
					ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);				
					if ( ret == GST_STATE_CHANGE_FAILURE )
					{
						LOGE("gst_element_set_state() failed\n");
						GstMessage *msg=NULL; /*for getting message from gst if any*/
						GstBus *bus=NULL;							
						/*timeout of 1 sec to get message from bus if any*/
						GstClockTime timeount_ns = GST_CLOCK_TIME_NONE;
						/*any final words from pipeline??...*/
						bus = gst_element_get_bus(pipeline);
						msg = gst_bus_timed_pop_filtered (bus, timeount_ns, GST_MESSAGE_ERROR | GST_MESSAGE_EOS );
						if( msg != NULL )
						{
							GError *err;
							gchar *debug_info;
		
							switch(GST_MESSAGE_TYPE(msg))
							{
								case GST_MESSAGE_ERROR:
									gst_message_parse_error(msg, &err, &debug_info);
									LOGE("error recieved from element %s : %s\n",GST_OBJECT_NAME(msg->src),err->message);
									LOGE("Debug info : %s\n", debug_info ? debug_info : "none");
									g_clear_error(&err);
									g_free(debug_info);
									break;
			
								case GST_MESSAGE_EOS:
									LOGD("End-of-Stream reached\n");
									break;
			
								default:
									LOGE("Unexpected message recieved\n");
									break;							
							}
							gst_message_unref(msg);
						}
						gst_object_unref(bus);
						gst_object_unref(pipeline);
						 /*setting NULL to make sure we can start streaming next time*/					
						pipeline=NULL;							
					}
					else
					{
						LOGD("Streaming started\n");
						rc = EXIT_SUCCESS;
					}
				}							
			} //==> if pipline != NULL 
			else
			{
				LOGE("Streaming already in progress\n");
			} 
			break;	//==> case : STREAM_START
					
		case STREAM_STOP:
			/* 
			 * if pipeline is not NULL than streaming is in progress
			 * note the usage of static variable here
			 */
			if(pipeline != NULL)
			{
				GstMessage *msg=NULL; /*for getting message from gst if any*/
				GstBus *bus=NULL;
				/*GST_STATE_NULL will stop the stream*/			
				ret = gst_element_set_state(pipeline,GST_STATE_NULL);
				if( ret == GST_STATE_CHANGE_FAILURE )
				{
					LOGE("gst_element_set_state() failed for GST_STATE_NULL\n");
				}
				else
				{
					/*timeout of 1 sec to get message from bus if any*/
					GstClockTime timeount_ns = 1000000000;
					/*any final words from pipeline??...*/
					bus = gst_element_get_bus(pipeline);
					msg = gst_bus_timed_pop_filtered (bus, timeount_ns, GST_MESSAGE_ERROR | GST_MESSAGE_EOS );
					if( msg != NULL )
					{
						GError *err;
						gchar *debug_info;
			
						switch(GST_MESSAGE_TYPE(msg))
						{
							case GST_MESSAGE_ERROR:
								gst_message_parse_error(msg, &err, &debug_info);
								LOGE("error recieved from element %s : %s\n",GST_OBJECT_NAME(msg->src),err->message);
								LOGE("Debug info : %s\n", debug_info ? debug_info : "none");
								g_clear_error(&err);
								g_free(debug_info);
								break;
				
							case GST_MESSAGE_EOS:
								LOGD("End-of-Stream reached\n");
								break;
				
							default:
								LOGE("Unexpected message recieved\n");
								break;							
						}
						gst_message_unref(msg);
					}
					gst_object_unref(bus);
					gst_object_unref(pipeline);
					 /*setting NULL to make sure we can start streaming next time*/					
					pipeline=NULL;
					rc = EXIT_SUCCESS;
				}
			}
			else
			{
				LOGE("Streaming not started yet\n");
			}
			break; //==> case : STREAM_STOP
	}
	
	return rc;						
}
#endif

/**
 * @brief : Enables/Disables video on LCD using network stream port no.
 * @param	: operation [in] Start or Stop operation
 * @param	: port [in] port no. to use
 * @returns : On successful operation, returns EXIT_SUCCESS else EXIT_FAILURE
 */
int camera_vid_lcd(stream_cmd operation, int port)
{
	/*Keeping pipeline static varible to keep track of streaming status
	 * if it is NULL, than no streaming is going on
	 * if it is not NULL, than streaming is in progress.
	 * This will avoid usage of a global pipeline pointer variable.
	 */
	static GstElement *pipeline = NULL;
	int rc = EXIT_FAILURE;
	GstStateChangeReturn ret;	
	
	switch(operation)
	{
		case STREAM_START:
			/*if pipeline is NULL, then we need to start streaming*/
			if( pipeline == NULL )
			{			
				char cmd[1024];
				memset(cmd,0,sizeof cmd);
				/*generating pipeline string from supplied arguments
				 *this can be directly passed to gst_parse_launch
				 */				
				sprintf(cmd,"tcpclientsrc port=%d !  multipartdemux   ! jpegdec  ! ffmpegcolorspace ! ximagesink",port);
				LOGV("[%s:%s:%d] pipeline : %s",__FILE__,__func__,__LINE__,cmd);

				/*init gstremer, no args for gst*/
				gst_init(NULL,NULL);
				/*set DISPLAY variable*/
				setenv("DISPLAY",":0.0",1);
				/*creating actual pipeline element.*/
				pipeline = gst_parse_launch(cmd,NULL);
				if(pipeline == NULL)					
				{
					LOGE("[%s:%s:%d] gst_parse_launch() failed.\n",__FILE__,__func__,__LINE__);
				}
				else
				{
					LOGV("[%s:%s:%d] Seting state to playing",__FILE__,__func__,__LINE__);
					/*setting GST_STATE_PLAYING will start streaming.*/
					ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);				
					if ( ret == GST_STATE_CHANGE_FAILURE )
					{
						LOGE("[%s:%s:%d] gst_element_set_state() failed\n",__FILE__,__func__,__LINE__);							
					}
					else
					{
						LOGD("[%s:%s:%d] Streaming started\n",__FILE__,__func__,__LINE__);
						rc = EXIT_SUCCESS;
					}
				}											
			}
			else
			{
				LOGE("[%s:%s:%d] Streaming already in progress\n",__FILE__,__func__,__LINE__);
			}
			break;

		case STREAM_STOP:
			/* 
			 * if pipeline is not NULL than streaming is in progress
			 * note the usage of static variable here
			 */		
			if(pipeline != NULL)
			{
				GstMessage *msg=NULL; /*for getting message from gst if any*/
				GstBus *bus=NULL;
				/*GST_STATE_NULL will stop the stream*/										
				ret = gst_element_set_state(pipeline,GST_STATE_NULL);
				if( ret == GST_STATE_CHANGE_FAILURE )
				{
					LOGE("[%s:%s:%d] gst_element_set_state() failed for GST_STATE_NULL\n",__FILE__,__func__,__LINE__);
				}
				else
				{
					/*timeout of 1 sec to get message from bus if any*/				
					GstClockTime timeount_ns = 1000000000;
					/*any final words from pipeline??...*/
					bus = gst_element_get_bus(pipeline);
					msg = gst_bus_timed_pop_filtered (bus, timeount_ns, GST_MESSAGE_ERROR | GST_MESSAGE_EOS );
					if( msg != NULL )
					{
						GError *err;
						gchar *debug_info;
			
						switch(GST_MESSAGE_TYPE(msg))
						{
							case GST_MESSAGE_ERROR:
								gst_message_parse_error(msg, &err, &debug_info);
								LOGE("[%s:%s:%d] error recieved from element %s : %s\n",__FILE__,__func__,__LINE__,GST_OBJECT_NAME(msg->src),err->message);
								LOGE("[%s:%s:%d] Debug info : %s\n",__FILE__,__func__,__LINE__, debug_info ? debug_info : "none");
								g_clear_error(&err);
								g_free(debug_info);
								break;
				
							case GST_MESSAGE_EOS:
								LOGD("[%s:%s:%d] End-of-Stream reached\n",__FILE__,__func__,__LINE__);
								break;
				
							default:
								LOGE("[%s:%s:%d] Unexpected message recieved\n",__FILE__,__func__,__LINE__);
								break;							
						}
						gst_message_unref(msg);
					}
					gst_object_unref(bus);
					gst_object_unref(pipeline);
					 /*setting NULL to make sure we can start streaming next time*/										
					pipeline=NULL;
					rc = EXIT_SUCCESS;
				}
			}
			else
			{
				LOGE("[%s:%s:%d] Streaming not started yet\n",__FILE__,__func__,__LINE__);
			}
			break;
	}
	return rc;
}

int camera_caputure_image(int port)
{
	int rc=EXIT_FAILURE;
	GstElement *pipeline = NULL;
	GstStateChangeReturn ret;	

	char cmd[1024];
	memset(cmd,0,sizeof cmd);
	/*generating pipeline string from supplied arguments
	 *this can be directly passed to gst_parse_launch
	 */				
	sprintf(cmd,"tcpclientsrc port=%d num-buffers=1 !  multipartdemux   ! jpegdec  ! ffmpegcolorspace ! filesink location=/root/captured/capture.jpg",port);
	LOGV("[%s:%s:%d] pipeline : %s",__FILE__,__func__,__LINE__,cmd);

	/*init gstremer, no args for gst*/
	gst_init(NULL,NULL);
	/*creating actual pipeline element.*/
	pipeline = gst_parse_launch(cmd,NULL);
	if(pipeline == NULL)					
	{
		LOGE("[%s:%s:%d] gst_parse_launch() failed.\n",__FILE__,__func__,__LINE__);
	}
	else
	{
		GstMessage *msg=NULL; /*for getting message from gst if any*/
		GstBus *bus=NULL;
		/*GST_STATE_NULL will stop the stream*/										
		ret = gst_element_set_state(pipeline,GST_STATE_PLAYING);
		if( ret == GST_STATE_CHANGE_FAILURE )
		{
			LOGE("[%s:%s:%d] gst_element_set_state() failed for GST_STATE_NULL\n",__FILE__,__func__,__LINE__);
		}
		else
		{
			/*timeout of 1 sec to get message from bus if any*/				
			GstClockTime timeount_ns = GST_CLOCK_TIME_NONE;
			/*any final words from pipeline??...*/
			bus = gst_element_get_bus(pipeline);
			msg = gst_bus_timed_pop_filtered (bus, timeount_ns, GST_MESSAGE_ERROR | GST_MESSAGE_EOS );
			if( msg != NULL )
			{
				GError *err;
				gchar *debug_info;
	
				switch(GST_MESSAGE_TYPE(msg))
				{
					case GST_MESSAGE_ERROR:
						gst_message_parse_error(msg, &err, &debug_info);
						LOGE("[%s:%s:%d] error recieved from element %s : %s\n",__FILE__,__func__,__LINE__,GST_OBJECT_NAME(msg->src),err->message);
						LOGE("[%s:%s:%d] Debug info : %s\n",__FILE__,__func__,__LINE__, debug_info ? debug_info : "none");
						g_clear_error(&err);
						g_free(debug_info);
						break;
		
					case GST_MESSAGE_EOS:
						rc = EXIT_SUCCESS;					
						LOGD("[%s:%s:%d] End-of-Stream reached\n",__FILE__,__func__,__LINE__);
						break;
		
					default:					
						LOGE("[%s:%s:%d] Unexpected message recieved\n",__FILE__,__func__,__LINE__);
						break;							
				}
				gst_message_unref(msg);
			}
			gst_object_unref(bus);
			gst_object_unref(pipeline);
			 /*setting NULL to make sure we can start streaming next time*/										
			pipeline=NULL;
		}
	}
	return rc;											
}

#if 0
int print_menu()
{

	printf("1 > check RCA video device\n");
	printf("2 > check UVC video device\n");
	printf("3 > Start RCA stream\n");
	printf("4 > Stop RCA stream\n");
	printf("5 > Start UVC stream\n");
	printf("6 > Stop UVC stream\n");
	printf("7 > Enable on LCD\n");
	printf("8 > Disable on LCD\n");	
	printf("=====================\n");
	printf("== Enter your choice : ");

	return 0;
	
}

int main()
{
	char video_dev[256];
	memset(video_dev,0,sizeof video_dev);
	int choice;
	//camera_get_device(video_dev,sizeof video_dev,UVC);	

	while(1)
	{
		print_menu();
		scanf("%d",&choice);		
		switch(choice)
		{
			case 1:
				camera_get_device(video_dev,sizeof video_dev,RCA);
				break;
			case 2:
				camera_get_device(video_dev,sizeof video_dev,UVC);			
				break;
			case 3:
				camera_get_device(video_dev,sizeof video_dev,RCA);			
//				camera_rca_stream(STREAM_START,video_dev,1234);
				camera_stream(STREAM_START,video_dev,1234,RCA);
				break;
			case 4:
				camera_get_device(video_dev,sizeof video_dev,RCA);			
//				camera_rca_stream(STREAM_STOP,video_dev,1234);			
				camera_stream(STREAM_STOP,video_dev,1234,RCA);
				break;
			case 5:
				camera_get_device(video_dev,sizeof video_dev,UVC);			
//				camera_uvc_stream(STREAM_START,video_dev,1234);			
				camera_stream(STREAM_START,video_dev,1234,UVC);
				break;
			case 6:
				camera_get_device(video_dev,sizeof video_dev,UVC);			
//				camera_uvc_stream(STREAM_STOP,video_dev,1234);			
				camera_stream(STREAM_STOP,video_dev,1234,UVC);
				break;
			case 7:
				camera_vid_lcd(STREAM_START,1234);
				break;
			case 8:
				camera_vid_lcd(STREAM_STOP,1234);
				break;
		}
	}

	return 0;
}



#endif
