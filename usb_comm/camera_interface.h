/**
 * @file	: camera_interface.h
 * @date	: 2013-12-31
 * @author	: Pratik Rathod
 * @brief	: structures, macros, enums for camera
 *
 */

#ifndef H_CAMERA_INTERFACE
#define H_CAMERA_INTERFACE

//! enum for camera types
typedef enum {
	RCA=0, //! for RCA camera
	UVC //! for UVC camera (USB camera)
} camera_type;

//! enums for video streaming control
typedef enum _stream_cmd{
	STREAM_START=0, //! start video streaming
	STREAM_STOP //! stop video streaming
}stream_cmd;

/**
 * @brief : gets the device node for video device type specified in argument cam
 * @param	: video_dev [out] buffer to write video device name
 * @param	: size [in] size of the input buffer
 * @param	: cam [in] type of camera, RCA or UVC 
 * @returns : On successful operation EXIT_SUCCESS, else EXIT_FAILURE
 */
int camera_get_device(char *video_dev,int size,camera_type cam);

/**
 * @brief : Start or Stop video stream over tcp network on specified ip and port.
 * @param	: operation [in] Start or Stop operation
 * @param	: video_dev [in] video device to use i.e. /dev/videoX
 * @param	: port [in] port no. 
 * @param	: cam [in] Camera device to use, either RCA or UVC
 * @returns : On successful operation returns EXIT_SUCCESS, else EXIT_FAILURE
 */
int camera_stream(stream_cmd operation, char *video_dev, int port,camera_type cam);

/**
 * @brief : Enables/Disables video on LCD using network stream port no.
 * @param	: operation [in] Start or Stop operation
 * @param	: port [in] port no. to use
 * @returns : On successful operation, returns EXIT_SUCCESS else EXIT_FAILURE
 */
int camera_vid_lcd(stream_cmd operation, int port);

#endif
