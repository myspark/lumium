/**
 * @file	: msg_queue.c
 * @date	: 2013-11-13
 * @author	: Pratik Rathod
 * @brief	: Functions defined here are responsible to create/manage fifo queue
 *            based on singly linked list.
 **/	

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>
#include "common.h"
#include "msg_queue.h"

//! Mutex & its initialization to prevent multi-thread access problems
pthread_mutex_t g_qmutex = PTHREAD_MUTEX_INITIALIZER;

/**
 * @brief : Initializes a fifo message queue. It will use malloc, so please free
 *           the returned pointer.
 * @returns : On successful operation it will return the pointer to fifo queue info
 *            structure. On error NULL. 
 */
queue_info *msg_queue_create()
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	
	queue_info *q = (queue_info *) malloc(sizeof(queue_info));
	if( q != NULL )
	{
		q->head = q->tail = NULL;
	}
	else
	{
		LOGE("[%s:%d:%s] malloc error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
	}
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);			
	return q;
}

/**
 * @brief : Adds a new entry at the tail of fifo queue.
 * @param	: q [in] pointer to fifo queue info structure
 * @param	: data [in] buffer containing data to be added
 * @param	: size [in] size of buffer 
 * @returns : On successful operation returns EXIT_SUCCESS else EXIT_FAILURE
 */
int msg_queue_add_data(queue_info *q, char *data, int size)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int rc = EXIT_FAILURE;
	MUTEX_LOCK(&g_qmutex);
	// if size of data is larger than fifo data size, reject the add call.
	if( size < MAX_DATA )
	{
		queue_data *q_data = (queue_data *) malloc (sizeof(*q_data));
		if(q_data!=NULL)
		{
			//temporary pointer
			queue_data *temp;
			memset(q_data,0,sizeof(*q_data));
			
			if( (q->head  == NULL ) && ( q->tail == NULL) )
			{
				//if list is empty, Add and make head and tail the same. 
				q->head = q->tail = q_data;
				memcpy(q->tail->data,data,size);
				q->tail->data_size = size;
				LOGD("[%s:%d:%s] New data added : [%s]:<%d> \n",__FILE__,__LINE__,__func__,q->tail->data,q->tail->data_size);
				q->tail->next=NULL;
				rc = EXIT_SUCCESS;	
			}
			else
			{
				//else Add & just update the tail pointer
				temp = q->tail;
				q->tail = q_data;
				memcpy(q->tail->data,data,size);
				q->tail->data_size = size;
				LOGD("[%s:%d:%s] New data added : [%s]:<%d> \n",__FILE__,__LINE__,__func__,q->tail->data,q->tail->data_size);
				temp->next = q->tail;			
				q->tail->next=NULL;				
				rc = EXIT_SUCCESS;
			}
		}
		else
		{
			LOGE("[%s:%d:%s] malloc error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
		}		
	}
	else
	{
		LOGE("[%s:%d:%s] size out of bound [%d > %d]\n",__FILE__,__LINE__,__func__,size,MAX_DATA);
	}
	MUTEX_UNLOCK(&g_qmutex);
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);		
	return rc;
}

/**
 * @brief : Reads the data from head of the fifo queue and stores it to data buffer
 *          pointer.
 * @param	: q [in] pointer to fifo queue info structure
 * @param	: data [out] data buffer to load the read data from head
 * @param	: size [in] size of data buffer 
 * @returns : On successful operation returns size of data buffer stored in fifo queue
 *            else -1.
 */
int msg_queue_read_head(queue_info *q, char *data, int size)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int rc = -1;
	memset(data,0,size);
	MUTEX_LOCK(&g_qmutex);	
	if(q != NULL)
	{
		if( q->head == NULL )
		{
			LOGV("[%s:%d:%s] Queue is empty\n",__FILE__,__LINE__,__func__);
		}
		else
		{			
			memcpy(data,q->head->data,size);
			rc = q->head->data_size;
		}
	}
	else
	{
		LOGE("[%s:%d:%s] Queue not initialized\n",__FILE__,__LINE__,__func__);		
	}
	MUTEX_UNLOCK(&g_qmutex);
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);		
	return rc;
}

/**
 * @brief : Frees the head entry from fifo queue and updates the head pointer.
 * @param	: q [in] pointer to fifo queue info structure
 * @returns : On successful operation returns EXIT_SUCCESS else EXIT_FAILURE.
 */
int msg_queue_del_head(queue_info *q)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int rc = EXIT_FAILURE;
	MUTEX_LOCK(&g_qmutex);	
	if(q != NULL)
	{
		if( q->head == NULL )
		{
			LOGD("[%s:%d:%s] Queue is empty\n",__FILE__,__LINE__,__func__);
		}
		else
		{
			queue_data *temp = q->head->next;
			free(q->head);
			q->head = temp;
			if ( q->head == NULL )
				q->tail = NULL;
			rc = EXIT_SUCCESS;
		}
	}
	else
	{
		LOGE("[%s:%d:%s] Queue not initialized\n",__FILE__,__LINE__,__func__);		
	}
	MUTEX_UNLOCK(&g_qmutex);
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);		
	return rc;
}

/**
 * @brief : Flushes whole fifo queue from memory
 * @param	: q [in] pointer to fifo queue info structure
 */
void msg_queue_flush(queue_info *q)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	queue_data *temp,*to_free;
	MUTEX_LOCK(&g_qmutex);
	temp = q->head;	
	for(;temp!=NULL;)
	{
		to_free=temp;
		temp=temp->next;		
		free(to_free);
		to_free=NULL;
	}
	q->head = q->tail = NULL;
	MUTEX_UNLOCK(&g_qmutex);
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);		
	return;
}

/**
 * @brief : Prints whole fifo queue data.
 * @param	: q [in] pointer to fifo queue structure
 */
void msg_queue_print(queue_info *q)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	queue_data *temp;
	int i;
	MUTEX_LOCK(&g_qmutex);
	temp = q->head;
	for(i=0;temp!=NULL;i++)
	{
		LOGD("[%s:%d:%s] Data[%d] : %s\n",__FILE__,__LINE__,__func__,i,temp->data);
		temp=temp->next;		
	}
	MUTEX_UNLOCK(&g_qmutex);	
	LOGD("[%s:%d:%s] Queue size : [%d]\n",__FILE__,__LINE__,__func__,i);
	LOGF("[%s:%d:%s] Leaving\n",__FILE__,__LINE__,__func__);	
	return;
}
