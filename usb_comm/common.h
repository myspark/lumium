/**
 * @file	: common.h
 * @date	: 2013-11-13
 * @author	: Pratik Rathod
 * @brief	: Common macro definitions
 **/
#ifndef H_COMMONH
#define H_COMMONH

//! Default port number of TCP server
#define PORT				9876

//! port no. for video streaming
#define STREAM_PORT			1234

//! ip addr for video streaming
#define STREAM_IP			"10.10.10.3"

//! default IP address of TCP server
#define IP					"10.10.10.3"

//! captured image from camera
#define IMAGE_PATH			"/home/root/capture.jpg"

//! default time out for receive loop
#define RECV_TIMEOUT_MS		3000

//! default timeout for uart loop
#define UART_TIMEOUT_MS		5000	

//! maximum no. of uart
#define MAX_UART			1

//! uart 1 named pipe prefix with path
#define UART_1_FIFO_PREFIX	"/tmp/uartd_ttyS2_"
//! uart 2 named pipe prefix with path
#define UART_2_FIFO_PREFIX	"/tmp/uartd_ttyO0_"

//! mutext lock
#define MUTEX_LOCK(a)		pthread_mutex_trylock(a)

//! mutex unlock
#define MUTEX_UNLOCK(a)		pthread_mutex_unlock(a)

//! debug log
#define LOGD(...)			printf(__VA_ARGS__)
//! error log
#define LOGE(...)			printf(__VA_ARGS__)
//#define LOGV(...)			printf(__VA_ARGS__)
//! verbose log
#define LOGV(...)			(void)0

//#define LOGF(...)			printf(__VA_ARGS__)
#define LOGF(...)			(void)0

//! different return error codes 
typedef enum _return_type{
	NO_ERROR = EXIT_SUCCESS, //! no error
	INVALID_CHECKSUM, //! invalid checksum while parsing command
	DATA_OUT_OF_BOUND, //! data out of bound while parsing command
	UNKNOWN_ERROR //! unknown error
}return_type;

#endif
