/**
 * @file	: usb_camera_control.c
 * @date	: 2013-12-03
 * @author	: Pratik Rathod
 * @brief	:  Contains functions that controls usb webcam operations.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <gst/gst.h>
#include "common.h"
#include "usb_camera_control.h"

/**
 * @brief : Start or Stop video stream over tcp network on specified ip and port.
 * @param	: operation [in] Start or Stop operation
 * @param	: ip [in] Ip address
 * @param	: port [in] port no. 
  * @param	: video_dev [in] video device to use i.e. /dev/videoX
 * @returns : On successful operation returns EXIT_SUCCESS, else EXIT_FAILURE
 */
int usb_camera_vid_stream(stream_cmd operation, char *ip, int port, char *video_dev)
{
	/*Keeping pipeline static varible to keep track of streaming status
	 * if it is NULL, than no streaming is going on
	 * if it is not NULL, than streaming is in progress.
	 * This will avoid usage of a global pipeline pointer variable.
	 */
	static GstElement *pipeline = NULL;
	GstStateChangeReturn ret;	/*we want the status change return from gst*/
	int rc = EXIT_FAILURE;
	
	switch(operation)
	{
		case STREAM_START:
			/*if pipeline is NULL, then we need to start streaming*/
			if( pipeline == NULL )
			{
				/*ip and video strings are must required.*/
				if( ( ip == NULL ) || ( video_dev == NULL) )
				{
					LOGE("null data in ip or video device name\n");					
				}
				else
				{
					char cmd[1024];
					memset(cmd,0,sizeof cmd);
					/*generating pipeline string from supplied arguments
					 *this can be directly passed to gst_parse_launch
					 */
					sprintf(cmd,"v4l2src device=%s ! video/x-raw-yuv,framerate=(fraction)30/1 ! ffmpegcolorspace  ! jpegenc ! multipartmux ! tcpserversink ip=%s port=%d", video_dev,ip,port);
	
					LOGV("pipeline : %s",cmd);
	
					/*init gstremer, no arguments for gst*/
					gst_init(NULL,NULL);
					/*creating actual pipeline element.*/
					pipeline = gst_parse_launch(cmd,NULL);
					if(pipeline == NULL)					
					{
						LOGE("gst_parse_launch() failed.\n");
					}
					else
					{
						LOGV("Seting state to playing");
						/*setting GST_STATE_PLAYING will start streaming.*/
						ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);				
						if ( ret == GST_STATE_CHANGE_FAILURE )
						{
							LOGE("gst_element_set_state() failed\n");							
						}
						else
						{
							LOGD("Streaming started\n");
							rc = EXIT_SUCCESS;
						}
					}							
				}
			}
			else
			{
				LOGE("Streaming already in progress\n");
			}
			break;

		case STREAM_STOP:
			/* 
			 * if pipeline is not NULL than streaming is in progress
			 * note the usage of static variable here
			 */
			if(pipeline != NULL)
			{
				GstMessage *msg=NULL; /*for getting message from gst if any*/
				GstBus *bus=NULL;
				/*GST_STATE_NULL will stop the stream*/			
				ret = gst_element_set_state(pipeline,GST_STATE_NULL);
				if( ret == GST_STATE_CHANGE_FAILURE )
				{
					LOGE("gst_element_set_state() failed for GST_STATE_NULL\n");
				}
				else
				{
					/*timeout of 1 sec to get message from bus if any*/
					GstClockTime timeount_ns = 1000000000;
					/*any final words from pipeline??...*/
					bus = gst_element_get_bus(pipeline);
					msg = gst_bus_timed_pop_filtered (bus, timeount_ns, GST_MESSAGE_ERROR | GST_MESSAGE_EOS );
					if( msg != NULL )
					{
						GError *err;
						gchar *debug_info;
			
						switch(GST_MESSAGE_TYPE(msg))
						{
							case GST_MESSAGE_ERROR:
								gst_message_parse_error(msg, &err, &debug_info);
								LOGE("error recieved from element %s : %s\n",GST_OBJECT_NAME(msg->src),err->message);
								LOGE("Debug info : %s\n", debug_info ? debug_info : "none");
								g_clear_error(&err);
								g_free(debug_info);
								break;
				
							case GST_MESSAGE_EOS:
								LOGD("End-of-Stream reached\n");
								break;
				
							default:
								LOGE("Unexpected message recieved\n");
								break;							
						}
						gst_message_unref(msg);
					}
					gst_object_unref(bus);
					gst_object_unref(pipeline);
					 /*setting NULL to make sure we can start streaming next time*/					
					pipeline=NULL;
					rc = EXIT_SUCCESS;
				}
			}
			else
			{
				LOGE("Streaming not started yet\n");
			}
			break;
	}
	return rc;
}

/**
 * @brief : Enables/Disables video on LCD using network stream port no.
 * @param	: operation [in] Start or Stop operation
 * @param	: port [in] port no. to use
 * @param	: <param1> [<in/out>] <description> 
 * @returns : On successful operation, returns EXIT_SUCCESS else EXIT_FAILURE
 */
int usb_camera_vid_lcd(stream_cmd operation, int port)
{
	/*Keeping pipeline static varible to keep track of streaming status
	 * if it is NULL, than no streaming is going on
	 * if it is not NULL, than streaming is in progress.
	 * This will avoid usage of a global pipeline pointer variable.
	 */
	static GstElement *pipeline = NULL;
	int rc = EXIT_FAILURE;
	GstStateChangeReturn ret;	
	
	switch(operation)
	{
		case STREAM_START:
			/*if pipeline is NULL, then we need to start streaming*/
			if( pipeline == NULL )
			{			
				char cmd[1024];
				memset(cmd,0,sizeof cmd);
				/*generating pipeline string from supplied arguments
				 *this can be directly passed to gst_parse_launch
				 */				
				sprintf(cmd,"tcpclientsrc port=%d !  multipartdemux   ! jpegdec  ! ffmpegcolorspace ! ximagesink",port);
				LOGV("pipeline : %s",cmd);

				/*init gstremer, no args for gst*/
				gst_init(NULL,NULL);
				/*creating actual pipeline element.*/
				pipeline = gst_parse_launch(cmd,NULL);
				if(pipeline == NULL)					
				{
					LOGE("gst_parse_launch() failed.\n");
				}
				else
				{
					LOGV("Seting state to playing");
					/*setting GST_STATE_PLAYING will start streaming.*/
					ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);				
					if ( ret == GST_STATE_CHANGE_FAILURE )
					{
						LOGE("gst_element_set_state() failed\n");							
					}
					else
					{
						LOGD("Streaming started\n");
						rc = EXIT_SUCCESS;
					}
				}											
			}
			else
			{
				LOGE("Streaming already in progress\n");
			}
			break;

		case STREAM_STOP:
			/* 
			 * if pipeline is not NULL than streaming is in progress
			 * note the usage of static variable here
			 */		
			if(pipeline != NULL)
			{
				GstMessage *msg=NULL; /*for getting message from gst if any*/
				GstBus *bus=NULL;
				/*GST_STATE_NULL will stop the stream*/										
				ret = gst_element_set_state(pipeline,GST_STATE_NULL);
				if( ret == GST_STATE_CHANGE_FAILURE )
				{
					LOGE("gst_element_set_state() failed for GST_STATE_NULL\n");
				}
				else
				{
					/*timeout of 1 sec to get message from bus if any*/				
					GstClockTime timeount_ns = 1000000000;
					/*any final words from pipeline??...*/
					bus = gst_element_get_bus(pipeline);
					msg = gst_bus_timed_pop_filtered (bus, timeount_ns, GST_MESSAGE_ERROR | GST_MESSAGE_EOS );
					if( msg != NULL )
					{
						GError *err;
						gchar *debug_info;
			
						switch(GST_MESSAGE_TYPE(msg))
						{
							case GST_MESSAGE_ERROR:
								gst_message_parse_error(msg, &err, &debug_info);
								LOGE("error recieved from element %s : %s\n",GST_OBJECT_NAME(msg->src),err->message);
								LOGE("Debug info : %s\n", debug_info ? debug_info : "none");
								g_clear_error(&err);
								g_free(debug_info);
								break;
				
							case GST_MESSAGE_EOS:
								LOGD("End-of-Stream reached\n");
								break;
				
							default:
								LOGE("Unexpected message recieved\n");
								break;							
						}
						gst_message_unref(msg);
					}
					gst_object_unref(bus);
					gst_object_unref(pipeline);
					 /*setting NULL to make sure we can start streaming next time*/										
					pipeline=NULL;
					rc = EXIT_SUCCESS;
				}
			}
			else
			{
				LOGE("Streaming not started yet\n");
			}
			break;
	}
	return rc;
}

int usb_camera_capture_image(char *file, char *video_dev)
{
	int rc = EXIT_FAILURE;
	
	/*ip and video strings are must required.*/
	if( ( video_dev == NULL) || ( file == NULL) )
	{
		LOGE("null data in ip/video/file name\n");					
	}
	else
	{
		GstElement *pipeline = NULL;
		GstStateChangeReturn ret;	/*we want the status change return from gst*/
		char cmd[1024];
		memset(cmd,0,sizeof cmd);
		/*generating pipeline string from supplied arguments
		 *this can be directly passed to gst_parse_launch
		 */
		sprintf(cmd,"v4l2src device=%s num-buffers=1 ! video/x-raw-yuv,framerate=(fraction)30/1 ! jpegenc !  filesink location=%s",video_dev, file);

		LOGD("pipeline : %s",cmd);

		/*init gstremer, no arguments for gst*/
		gst_init(NULL,NULL);
		/*creating actual pipeline element.*/
		pipeline = gst_parse_launch(cmd,NULL);
		if(pipeline == NULL)					
		{
			LOGE("gst_parse_launch() failed.\n");
		}
		else
		{
			LOGV("Seting state to playing");
			/*setting GST_STATE_PLAYING will start streaming.*/
			ret = gst_element_set_state(pipeline, GST_STATE_PLAYING);				
			if ( ret == GST_STATE_CHANGE_FAILURE )
			{
				LOGE("gst_element_set_state() failed\n");							
			}
			else
			{
				GstMessage *msg=NULL; /*for getting message from gst if any*/
				GstBus *bus=NULL;
				/*timeout of 1 sec to get message from bus if any*/
				GstClockTime timeount_ns = GST_CLOCK_TIME_NONE ;
				/*any final words from pipeline??...*/
				bus = gst_element_get_bus(pipeline);
				msg = gst_bus_timed_pop_filtered (bus, timeount_ns, GST_MESSAGE_ERROR | GST_MESSAGE_EOS );
				if( msg != NULL )
				{
					GError *err;
					gchar *debug_info;
		
					switch(GST_MESSAGE_TYPE(msg))
					{
						case GST_MESSAGE_ERROR:
							gst_message_parse_error(msg, &err, &debug_info);
							LOGE("error recieved from element %s : %s\n",GST_OBJECT_NAME(msg->src),err->message);
							LOGE("Debug info : %s\n", debug_info ? debug_info : "none");
							g_clear_error(&err);
							g_free(debug_info);
							break;
			
						case GST_MESSAGE_EOS:
							LOGD("End-of-Stream reached\n");
							rc = EXIT_SUCCESS;							
							break;
			
						default:
							LOGE("Unexpected message recieved\n");
							break;							
					}
					gst_message_unref(msg);
				}
				gst_element_set_state(pipeline, GST_STATE_PLAYING);
				gst_object_unref(bus);
				gst_object_unref(pipeline);
				 /*setting NULL to make sure we can start streaming next time*/					
				pipeline=NULL;
				rc = EXIT_SUCCESS;
			}
		}							
	}
	return rc;
}
