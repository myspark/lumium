var searchData=
[
  ['g_5fconfig',['g_config',['../config_8c.html#a964c836ad6616c89814bc72d8477a536',1,'config.c']]],
  ['g_5fqinfo',['g_qinfo',['../main_8c.html#aa8e35fa0d3af46cc30f7e55247884583',1,'g_qinfo():&#160;main.c'],['../loops_8c.html#aa8e35fa0d3af46cc30f7e55247884583',1,'g_qinfo():&#160;main.c']]],
  ['g_5fqmutex',['g_qmutex',['../msg__queue_8c.html#a9181c83e75f4b4510e54b42e0defe6b3',1,'msg_queue.c']]],
  ['g_5frecv_5floop_5fcontrol',['g_recv_loop_control',['../main_8c.html#a46b91c50272f030419ac706edf829a2c',1,'g_recv_loop_control():&#160;loops.c'],['../loops_8c.html#a46b91c50272f030419ac706edf829a2c',1,'g_recv_loop_control():&#160;loops.c']]],
  ['g_5fres_5floop_5fcontrol',['g_res_loop_control',['../main_8c.html#afc1c9f1e8c062b347365caf5ed3cc8d7',1,'g_res_loop_control():&#160;loops.c'],['../loops_8c.html#afc1c9f1e8c062b347365caf5ed3cc8d7',1,'g_res_loop_control():&#160;loops.c']]],
  ['g_5ftcpmutex',['g_tcpmutex',['../tcp__comm_8c.html#adcd9d5e99791d1240041a2a400e9cc4a',1,'tcp_comm.c']]],
  ['g_5fuart_5floop_5fcontrol',['g_uart_loop_control',['../main_8c.html#a477ae8d8e5a9e2cb538b70fdd2e68412',1,'g_uart_loop_control():&#160;loops.c'],['../loops_8c.html#a477ae8d8e5a9e2cb538b70fdd2e68412',1,'g_uart_loop_control():&#160;loops.c']]],
  ['get_5fcomm_5fport',['get_comm_port',['../config_8c.html#a137174c9429efa8c365956568d276eec',1,'get_comm_port():&#160;config.c'],['../config_8h.html#a137174c9429efa8c365956568d276eec',1,'get_comm_port():&#160;config.c']]],
  ['get_5fstream_5fport',['get_stream_port',['../config_8c.html#a2f00d171a6c7cd22599e71cf4171f58b',1,'get_stream_port():&#160;config.c'],['../config_8h.html#a2f00d171a6c7cd22599e71cf4171f58b',1,'get_stream_port():&#160;config.c']]],
  ['get_5fuart_5fprefix',['get_uart_prefix',['../config_8c.html#aa5f1d1fa98f39d63af879d9ab3eed9c6',1,'get_uart_prefix(char *buffer, int size):&#160;config.c'],['../config_8h.html#aa5f1d1fa98f39d63af879d9ab3eed9c6',1,'get_uart_prefix(char *buffer, int size):&#160;config.c']]]
];
