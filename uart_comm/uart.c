/**
 * @file	: uart.c
 * @date	: 07 - 11 -2013
 * @author  : Pratik Rathod
 * @brief	: Functions defined in this file are responsible for uart open,read,
 *			  write,close operations.
 **/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>
#include <string.h>
#include <errno.h>
#include <poll.h>
#include "common.h"

/**
 * @brief : Opens the provided uart port in name with baudrate = BAUD macro
 * @param	: name [in] uart port device name (full path)
 * @returns : on successful operation returns fd of opened uart port else -1
 */
int uart_open( char *name )
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int fd;
    struct termios new_termios;
    
    memset(&new_termios,0,sizeof(new_termios));
    
    fd=open(name, O_RDWR);
    if(fd<0)
    {
        LOGE("[%s:%d:%s] failed to open \"%s\" : [%s]\n",__FILE__,__LINE__,__func__,name,strerror(errno));
        return -1;
    }
    
    //standard serial port set-up parameters
    tcgetattr(fd,&new_termios);
    new_termios.c_iflag = 0;
    new_termios.c_oflag = 0;
    new_termios.c_cflag = CS8 | CREAD | CLOCAL;
    new_termios.c_lflag = 0;
    new_termios.c_cc[VMIN] = 1;
    new_termios.c_cc[VTIME] = 5;
    cfsetospeed(&new_termios,BAUD);
    cfsetispeed(&new_termios,BAUD);
    //set new termios
    tcsetattr(fd,TCSANOW,&new_termios);
    LOGD("[%s:%d:%s] %s device opened and configured.\n",__FILE__,__LINE__,__func__,name);
    return fd;	
}

/**
 * @brief : reads from uart port from provided fd and fills data in provided buffer
 * @param	: fd [in] file descriptor of uart port
 * @param	: data [out] buffer pointer to fill read data
 * @param	: size [in] size of the buffer 
 * @param	: timeout_ms [in] timeout for read in milliseconds.
 * @returns : On successful operation returns no. of bytes read, else -1. In case if
 *			   timeout occurs it will return 0.
 */
int uart_read(int fd, char *data,int size,int timeout_ms)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
	int index=0;

    if(fd>0)
    {
        //clearing the buffer, we do not want corrupted data
        memset(data,0,size);
    	int read_bytes=0,i=0;
   	    struct pollfd fds;
   	    fds.fd = fd;
   	    fds.events = POLLIN;
   	    
   	    i = poll(&fds,1,timeout_ms);
   	    if(i != 0)
   	    {
   	    	//some event occurred
   	    	if( ( fds.revents & POLLIN ) != 0 )
   	    	{
   	    		//It is a POLLIN event, data available to read
		   	   	do
				{
					read_bytes=read(fd,&data[index],1);
					if(read_bytes<0)
					{
						LOGE("[%s:%d:%s] read error : [%s]\n",__FILE__,__LINE__,__func__,strerror(errno));
						return -1;
					}
					#if 1
					LOGV("[%s:%d:%s] READ[%d] : %c [%x] \n",__FILE__,__LINE__,__func__,index,data[index],data[index]);
					#endif
					if( data[index] == '\n' || read_bytes == 0)
						break;
					index++;		        	
				}while(read_bytes > 0 );
			
				LOGV("[%s:%d:%s] %d bytes read : --> %s\n",__FILE__,__LINE__,__func__,index+1,data);		
   	    	}
   	    }
   	    else
   	    {
   	    	LOGV("[%s:%d:%s] read timeout\n",__FILE__,__LINE__,__func__);
   	    	return 0;
   	    }
    }
    else
    {
        LOGE("[%s:%d:%s] Not a valid uart fd\n",__FILE__,__LINE__,__func__);
        return -1;
    }    
    return (index+1);
}

/**
 * @brief : writes data to provided uart fd provided in buffer.
 * @param	: fd [in] file descriptor of uart
 * @param	: buffer [in] data buffer to write to uart
 * @param	: len [in] length of the data 
 * @returns : on successful operation returns no. of written bytes, else -1.
 */
int uart_write(int fd, char *buffer, int len)
{
	LOGV("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
    int write_bytes=0;
    if(buffer==NULL)
        return -1;
                        
    if(fd>0)
    {

        write_bytes = write(fd,buffer,len);
        if(write_bytes!=len)
        {
            LOGE("[%s:%d:%s] write error : %s\n",__FILE__,__LINE__,__func__,strerror(errno));
            return -1;
        }
        LOGV("[%s:%d:%s] %d bytes written : --> %s\n",__FILE__,__LINE__,__func__,write_bytes,buffer);
    }
    else
    {
        LOGE("[%s:%d:%s] gsm_serial device not opened\n",__FILE__,__LINE__,__func__);
        return EXIT_FAILURE;
    }
    return write_bytes;
}

/**
 * @brief : closes the uart device.
 * @param	: fd [in] file descriptor of uart to close
 * @returns : on successful operation returns EXIT_SUCCESS else EXIT_FAILURE
 */
int uart_close(int fd)
{
	LOGF("[%s:%d:%s] called\n",__FILE__,__LINE__,__func__);
    if(fd>0)
    {
        LOGD("[%s:%d:%s] Closing uart port.\n",__FILE__,__LINE__,__func__);
        close(fd);
        return EXIT_SUCCESS;
    }
    LOGE("[%s:%d:%s] uart is not opened.\n",__FILE__,__LINE__,__func__);
    return EXIT_FAILURE;
}
