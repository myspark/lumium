var searchData=
[
  ['uart_5fclose',['uart_close',['../uart_8c.html#abf60b929d80f43ae5238f2d6bec4647c',1,'uart_close(int fd):&#160;uart.c'],['../main_8c.html#abf60b929d80f43ae5238f2d6bec4647c',1,'uart_close(int fd):&#160;uart.c']]],
  ['uart_5fopen',['uart_open',['../uart_8c.html#a0b71b71ae58e407f65566bf20caf3d8a',1,'uart_open(char *name):&#160;uart.c'],['../main_8c.html#a0b71b71ae58e407f65566bf20caf3d8a',1,'uart_open(char *name):&#160;uart.c']]],
  ['uart_5fread',['uart_read',['../uart_8c.html#a1399cacbabb66ae60b41768fb2b5245d',1,'uart_read(int fd, char *data, int size, int timeout_ms):&#160;uart.c'],['../loops_8c.html#a1399cacbabb66ae60b41768fb2b5245d',1,'uart_read(int fd, char *data, int size, int timeout_ms):&#160;uart.c'],['../main_8c.html#a1399cacbabb66ae60b41768fb2b5245d',1,'uart_read(int fd, char *data, int size, int timeout_ms):&#160;uart.c']]],
  ['uart_5fwrite',['uart_write',['../uart_8c.html#a028085af5798594be87faa01f549a143',1,'uart_write(int fd, char *buffer, int len):&#160;uart.c'],['../loops_8c.html#a028085af5798594be87faa01f549a143',1,'uart_write(int fd, char *buffer, int len):&#160;uart.c'],['../main_8c.html#a028085af5798594be87faa01f549a143',1,'uart_write(int fd, char *buffer, int len):&#160;uart.c']]]
];
