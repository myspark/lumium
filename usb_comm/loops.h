/**
 * @file	: loops.h
 * @date	: 2013-11-13
 * @author	: Pratik Rathod
 * @brief	: Function declarations of loops.c
 **/

#ifndef H_LOOPSH
#define H_LOOPSH

/**
 * @brief : Infinite loop to receive the data from TCP server application and add
 *          them to fifo queue for response loop to take care of it.
 * @param	: *fd [in] pointer of tcp connected socket file descriptor
 */
void *recv_loop(void *fd);

/**
 * @brief : Infinite loop function to respond on TCP servers' messages. It reads 
 *           the head of queue and performs according action.
 * @param	: *fd [in] pointer to tcp connected socket file descriptor
 */
void *respond_loop(void *fd);

/**
 * @brief : Infinite loop function to monitor uart read fifos for incoming uart data
 *          and pass it to TCP server
 * @param	: *fd [in] pointer to connected TCP socket descriptor
 */
void *uart_loop(void *fd);	

/**
 * @brief : reads the uart response from read pipe with timeout of UART_TIMEOUT_MS
 *          if any data available then it directly sends over network
 * @param	: sockfd [in] socket fd of TCP communication,
 * @param	: <param1> [<in/out>] <description>
 * @param	: <param1> [<in/out>] <description> 
 * @returns : On successful operation returns EXIT_SUCCESS, else in case of timeout or
 *            any other error returns EXIT_FAILURE
 */
int uart_operation(int sockfd);
	
#endif
