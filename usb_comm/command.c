/**
 * @file	: command.c
 * @date	: 2013-12-03
 * @author	: Pratik Rathod
 * @brief	: fuctions for command related operations 
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "common.h"
#include "command.h"
#include "tcp_comm.h"
#include "camera_interface.h"
#include "config.h"

/**
 * @brief : gets XOR checksum of provided data buffer
 * @param	: data [in] buffer to calculate the XOR checksum of.
 * @param	: size [in] size of the buffer
 * @returns : returns XOR checksum.
 */
char command_xor_chksm(const char *data, int size)
{
	char chksum = 0;
	int i;
	for(i=0; i<size; i++)
	{
		chksum = chksum ^ data[i];
	}
	LOGD("[%s:%s:%d] chksum : %x\n",__FILE__,__func__,__LINE__,chksum);
	return chksum;
}

/**
 * @brief : parses the data buffer recieved in to struct
 * @param	: c [out] cmd struct to fill in the parsed data
 * @param	: data [in] buffer data recieved
 * @param	: size [in] size of buffer 
 * @returns : On successful operation if NO_ERROR, else error type will be returned.
 */
return_type command_parse(cmd *c,const char *data, int size)
{
	int index=0,i;
	int rc = UNKNOWN_ERROR;
	/*get checksum included in buffer data*/
	c->chksum = data[size-1];
	/*verify it with calculating checksume and compare it.*/
	if( c->chksum == command_xor_chksm(data,size-1) )
	{
		/*parsing header*/
		c->header = data[index++];
		/*getting the length of message*/
		c->len = 0;
		memcpy(&(c->len),&data[index],1);
		LOGD("[%s:%s:%d] Length : %d{%x}\n",__FILE__,__func__,__LINE__,c->len,c->len);
		index++;
		/*parsing the command code*/
		c->command = data[index++];
		/*if total length is >4 than there is extra data in recieved buffer
		 * x--------x--------x--------------x------------x
		 * | Header | length | command code | XOR chksum | <-- length =< 4
		 * x--------x--------x--------------x------------x
		 *		
		 * x--------x--------x--------------x------------x------------x
		 * | Header | length | command code | extra data | XOR chksum | <-- length > 4
		 * x--------x--------x--------------x------------x------------x
		 */
		if(c->len > 4)
		{
			int a_size = c->len - 4;
			//there is some data.
			if(a_size < MAX_COMMAND_DATA )
			{
				memcpy(c->data,&data[index],a_size );
				for(i=0;i<a_size;i++)
					LOGD("[%s:%s:%d] Data : %c {%x}\n",__FILE__,__func__,__LINE__,c->data[i],c->data[i]);
				rc = NO_ERROR;
			}
			else
			{
				LOGE("[%s:%s:%d] data out of bound\n",__FILE__,__func__,__LINE__);
				rc = DATA_OUT_OF_BOUND;
			}
		}
		else
		{
			LOGD("[%s:%s:%d] No data\n",__FILE__,__func__,__LINE__);
			rc = NO_ERROR;
		}
	}
	else
	{
		LOGE("[%s:%s:%d] Invalid checksum\n",__FILE__,__func__,__LINE__);
		rc = INVALID_CHECKSUM;
	}
	return rc;
}

/**
 * @brief : performs actions according to supplied struct cmd.
 * @param	: c [in] cmd struct
 * @param	: sockfd [in] TCP socket fd to send responce
 * @returns : On successful operation returns EXIT_SUCCESS, else EXIT_FAILURE
 */
int command_action(cmd c, int sockfd)
{
	int rc = EXIT_FAILURE;
	/*getting the command code*/
	cmd_type type = (cmd_type) c.command;
	switch( type )
	{
		/*start video on LCD screen*/
		case ENABLE_VIDEO_STREAM:
			LOGD("[%s:%s:%d] ENABLE_VIDEO_STREAM\n",__FILE__,__func__,__LINE__);		
			if(sockfd>0)
			{
				char data[5];
				memset(data,0,sizeof data);
				data[0]='$';
				data[1]=0x05;
				data[2]=ENABLE_VIDEO_STREAM;
				camera_vid_lcd(STREAM_STOP,get_stream_port());
				if( camera_vid_lcd(STREAM_START,get_stream_port()) == EXIT_SUCCESS )
				{
					data[3]=ACK;				
				}
				else
				{
					data[3]=NACK;
				}
				data[4]=command_xor_chksm(data,4);
				/*send responce*/
				tcp_send_msg(sockfd,data,sizeof data);
			}
			rc = EXIT_SUCCESS;			
			break;
		
		/*capture and upload picture from camera*/	
		case UPLOAD_GEM_PRINT_IMAGE:
			if(sockfd>0)
			{
				char data[5];
				memset(data,0,sizeof data);
				data[0]='$';
				data[1]=0x05;
				data[2]=UPLOAD_GEM_PRINT_IMAGE;
#if 0			
				if( command_upload_image(sockfd) == EXIT_SUCCESS )
				{
					data[3]=ACK;
				}
				else
				{
					data[3]=NACK;
				}				
#else
				data[3]=ACK;
#endif				
				data[4]=command_xor_chksm(data,4);
				tcp_send_msg(sockfd,data,sizeof data);
			}
			LOGD("[%s:%s:%d] UPLOAD_GEM_PRINT_IMAGE\n",__FILE__,__func__,__LINE__);
			rc = EXIT_SUCCESS;						
			break;			
		
		/*actions related primary camera*/
		case PRIM_CAM:
			if(sockfd>0)
			{
				char data[5];
				memset(data,0,sizeof data);
				data[0]='$';
				data[1]=0x05;
				data[2]=PRIM_CAM;
				if(c.len > 4)
				{				
					/* disable camera*/
					if( c.data[0] == 0x00 )
					{
						char video_dev[256];
						memset(video_dev,0,sizeof video_dev);
						if( (camera_get_device(video_dev,sizeof video_dev,RCA) == EXIT_SUCCESS) &&
						(camera_stream(STREAM_STOP,video_dev, get_stream_port(),RCA) == EXIT_SUCCESS) )
						{
							data[3]=ACK;
							camera_vid_lcd(STREAM_STOP,get_stream_port());							
						}
						else
						{
							data[3]=NACK;						
						}
						LOGD("[%s:%s:%d] PRIM_CAM_DISABLE\n",__FILE__,__func__,__LINE__);					
					}
					else if ( c.data[0] == 0x01 ) /*enable camera*/
					{
						char video_dev[256];
						memset(video_dev,0,sizeof video_dev);
						if( (camera_get_device(video_dev,sizeof video_dev,RCA) == EXIT_SUCCESS) &&
						(camera_stream(STREAM_START,video_dev, get_stream_port(),RCA) == EXIT_SUCCESS) )
						{
							data[3]=ACK;
						}
						else
						{
							data[3]=NACK;						
						}
						LOGD("[%s:%s:%d] PRIM_CAM_ENABLE\n",__FILE__,__func__,__LINE__);
					}
					else /*invalid data*/
					{
						data[3]=NACK; 
					}
				}
				else
				{
					LOGE("[%s:%s:%d] Data value is required for PRIM_CAM operation\n",__FILE__,__func__,__LINE__);
					data[3]=NACK;				
				}
				
				data[4]=command_xor_chksm(data,4);				
				tcp_send_msg(sockfd,data,sizeof data);
			}
			rc = EXIT_SUCCESS;						
			break;	
		
		/*action related to secondary camera*/	
		case SEC_CAM:
			if(sockfd>0)
			{
				char data[5];
				char video_dev[256];				
				memset(data,0,sizeof data);
				data[0]='$';
				data[1]=0x05;
				data[2]=SEC_CAM;				
				if(c.len > 4)
				{
					if( c.data[0] == 0x00 ) /*disable camera*/
					{				
						memset(video_dev,0,sizeof video_dev);
						if( (camera_get_device(video_dev,sizeof video_dev,UVC) == EXIT_SUCCESS) &&
						(camera_stream(STREAM_STOP,video_dev, get_stream_port(),UVC) == EXIT_SUCCESS) )
						{
							data[3]=ACK;
							camera_vid_lcd(STREAM_STOP,get_stream_port());														
						}
						else
						{
							data[3]=NACK;						
						}
						LOGD("[%s:%s:%d] SEC_CAM_DISABLE\n",__FILE__,__func__,__LINE__);							
					}
					else if ( c.data[0] == 0x01 ) /*enable camera*/
					{
						memset(video_dev,0,sizeof video_dev);
						if( (camera_get_device(video_dev,sizeof video_dev,UVC) == EXIT_SUCCESS) &&
						(camera_stream(STREAM_START,video_dev, get_stream_port(),UVC) == EXIT_SUCCESS) )
						{
							data[3]=ACK;
						}
						else
						{
							data[3]=NACK;						
						}				
						LOGD("[%s:%s:%d] SEC_CAM_ENABLE\n",__FILE__,__func__,__LINE__);
					}
					else /*invalid data.*/
					{
						LOGE("[%s:%s:%d] Invalid data\n",__FILE__,__func__,__LINE__);
						data[3]=NACK;			
					}				
				}
				else
				{
					LOGE("[%s:%s:%d] Data value is required for SEC_CAM operation\n",__FILE__,__func__,__LINE__);
					data[3]=NACK;							
				}
				
				data[4]=command_xor_chksm(data,4);
				tcp_send_msg(sockfd,data,sizeof data);
			}
			rc = EXIT_SUCCESS;						
			break;
		
		/*disable all cameras.*/	
		case DISABLE_CAM:
			if(sockfd>0)
			{
				char data[5];
				char video_dev[256];
				memset(data,0,sizeof data);
				data[0]='$';
				data[1]=0x05;
				data[2]=DISABLE_CAM;
				/*disable camera video streaming*/
				memset(video_dev,0,sizeof video_dev);
				camera_get_device(video_dev,sizeof video_dev,UVC);
				camera_stream(STREAM_STOP,video_dev, get_stream_port(),UVC);
				camera_get_device(video_dev,sizeof video_dev,RCA);
				camera_stream(STREAM_STOP,video_dev, get_stream_port(),RCA);				
				/*disable video on lcd*/
				camera_vid_lcd(STREAM_STOP,get_stream_port());									
				data[3]=ACK;
				data[4]=command_xor_chksm(data,4);
				tcp_send_msg(sockfd,data,sizeof data);
			}
			LOGD("[%s:%s:%d] DISABLE_CAM\n",__FILE__,__func__,__LINE__);
			rc = EXIT_SUCCESS;						
			break;
			
		default:
			LOGD("[%s:%s:%d] Not an app command, forwarding to uart\n",__FILE__,__func__,__LINE__);
			rc = EXIT_FAILURE;
			break;
	}
	
	return rc;
}
#if 0
int command_upload_image(int sockfd)
{
	int rc = EXIT_FAILURE;
	int write_byte = 0;
	
	if ( usb_camera_capture_image(IMAGE_PATH,VIDEO_DEV) == EXIT_SUCCESS )
	{
		struct stat buf;
		memset(&buf,0,sizeof buf);
		if( stat(IMAGE_PATH,&buf) == 0 )
		{
			int in_fd = open(IMAGE_PATH,O_RDONLY);
			if(in_fd > 0)
			{
				write_byte = sendfile(sockfd,in_fd,NULL,buf.st_size);
				if( write_byte != buf.st_size )
				{
					LOGE("sendfile failed : %s",strerror(errno));
				}
				else
				{
					LOGD("%s is sent. %ld bytes written.",IMAGE_PATH,buf.st_size);
					close(in_fd);
					rc = EXIT_SUCCESS;
				}
			}
			else
			{
				LOGE("open failed for %s : %s",IMAGE_PATH,strerror(errno));
			}		
		}
		else
		{
			LOGE("stat for %s failed : %s",IMAGE_PATH,strerror(errno));
		}
	}
	return rc;
}
#endif
