/**
 * @file	: command.h
 * @date	: 2013-12-03
 * @author	: Pratik Rathod
 * @brief	: structures, macros, enums for cammand related actions
 *
 */
#ifndef H_COMMANDH
#define H_COMMANDH

//! max length of extra data received in message
#define MAX_COMMAND_DATA	1024

//! message format structure 
typedef struct message
{
	char header; //! header value
	int  len; //! length of whole message
	char command; //! command code in hex
	char data[MAX_COMMAND_DATA]; //! extra data if any
	char chksum; //! XOR checksum
} cmd;

//! different command codes 
typedef enum commands {
	ENABLE_VIDEO_STREAM = 0x51, //! enabled video on lcd
	UPLOAD_GEM_PRINT_IMAGE = 0x52, //! capture and upload image from camera
	PRIM_CAM = 0x65, //! primary camera actions
	SEC_CAM = 0x66, //! secondary camera actions
	DISABLE_CAM = 0x67 //! disable all cameras
} cmd_type;

//! responce types
enum res_type {
	ACK = 0x80, //! positive response
	NACK = 0x81 //! negative response
};

/**
 * @brief : gets XOR checksum of provided data buffer
 * @param	: data [in] buffer to calculate the XOR checksum of.
 * @param	: size [in] size of the buffer
 * @returns : returns XOR checksum.
 */
char command_xor_chksm(const char *data, int size);

/**
 * @brief : parses the data buffer recieved in to struct
 * @param	: c [out] cmd struct to fill in the parsed data
 * @param	: data [in] buffer data recieved
 * @param	: size [in] size of buffer 
 * @returns : On successful operation if NO_ERROR, else error type will be returned.
 */
return_type command_parse(cmd *c,const char *data, int size);

/**
 * @brief : performs actions according to supplied struct cmd.
 * @param	: c [in] cmd struct
 * @param	: sockfd [in] TCP socket fd to send responce
 * @returns : On successful operation returns EXIT_SUCCESS, else EXIT_FAILURE
 */
int command_action(cmd c, int sockfd);

/**
 * @brief : gets XOR checksum of provided data buffer
 * @param	: data [in] buffer to calculate the XOR checksum of.
 * @param	: size [in] size of the buffer
 * @returns : returns XOR checksum.
 */
char command_xor_chksm(const char *data, int size);

#endif
